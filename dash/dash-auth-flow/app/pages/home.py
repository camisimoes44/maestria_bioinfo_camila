import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import base64
from dash.dependencies import Output,Input,State
from dash import no_update
from flask_login import current_user
import time

from server import app
image_filename = 'img_home.png'
encoded_image = base64.b64encode(open(image_filename, 'rb').read())

home_login_alert = dbc.Alert(
    'User not logged in. Taking you to login.',
    color='danger'
)

def layout():
    return dbc.Container(
        dbc.Col(
            [
                dbc.Row([
                    dbc.Col([
                        dcc.Location(id='home-url', refresh=True),
                        html.Div(id='home-login-trigger', style=dict(display='none')),
                        html.Div(
                            [html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()), height="110px")],
                            style={'textAlign': 'center'}
                            )], className="mb-5 mt-5")
                ]),
                dbc.Row([
                    dbc.Col([html.H4('¡Bienvenida/o a actG-Learn!')], className="mb-4")
                ]),
                dbc.Row([
                    dbc.Col([
                        html.H5(
                            'Esta plataforma está destinada a la clasificación de variantes cortas germinales con dos objetivos principales: el aprendizaje de usuarios y el etiquetado de variantes destinado a un sistema de clasificación automática.'
                            )]
                    )
                ]),
                html.Br(),
                dbc.Row([
                    dbc.Col([
                        dbc.Card([
                            dbc.CardBody(
                                children=[
                                    dbc.Row([
                                        html.I(className='fas fa-thin fa-dna',style={'font-size': '30px', 'color':'#202a3d'}),
                                        html.H4(children=dbc.NavLink('Clasificación de variantes', href="/training"), className="text-center"
                                                )], justify="center"),
                                    #html.Br(),
                                    html.Hr(className="my-2"),
                                    #html.H4(children='Clasificación de variantes', className="text-center"),
                                    dbc.Row([html.H6(children='En esta sección se podrá realizar una práctica de '
                                                            'la clasificación de variantes con el objetivo de entrenar'
                                                            'al usuario en la tarea.',
                                                   )
                                           ],
                                          justify="center")
                                  ])
                        ], body=True, color="dark", outline=True)
                    ], width=6, className="mb-4"),
                    dbc.Col([
                        dbc.Card([
                            dbc.CardBody(
                                children=[
                                    dbc.Row([html.I(className='fas fa-solid fa-tags',
                                                    style={'font-size': '30px', 'color': '#202a3d'}),
                                             html.H4(children=
                                                     dbc.NavLink('Etiquetado de variantes', href="/classification"), className="text-center"
                                                     )], justify="center"),
                                    #html.H4(children='Etiquetado de variantes', className="text-center"),
                                    html.Hr(className="my-2"),
                                      dbc.Row([html.H6(
                                          children='En esta sección se podrán etiquetar variantes conflictivas'
                                                   'de interés, las cuales serán un insumo para un sistema'
                                                   'de clasificación automática'
                                          )
                                               ],
                                              justify="center"),
                                      ])
                        ], body=True, color="dark", outline=True)
                    ], width=6, className="mb-4"),
                ],
                    className="mb-5")
            ]
        )
    )


    # dbc.Row(
    # dbc.Col(
    #    [
    #        dcc.Location(id='home-url',refresh=True),
    #        html.Div(id='home-login-trigger',style=dict(display='none')),

    #        html.H1('Home page'),
    #        html.Br(),

    #        html.H5('Welcome to the home page!'),
    #        html.Br(),

    # html.Div(id='home-test-trigger'),
    # html.Div('before update',id='home-test')
    #    ],
    #    width=6
    # )

# @app.callback(
#    Output('home-test','children'),
#    [Input('home-test-trigger','children')]
# )

# def home_test_update(trigger):
#    '''
#    updates arbitrary value on home page for test
#    '''
#    time.sleep(2)
#    return html.Div('after the update',style=dict(color='red'))
