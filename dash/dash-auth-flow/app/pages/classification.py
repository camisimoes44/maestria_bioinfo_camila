import dash
import dash_table
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output,Input,State
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import numpy as np
from pandas import DataFrame
from flask_login import logout_user, current_user
from dash_extensions import Download
from dash_extensions.snippets import send_data_frame
import re


import config
import common
from server import app

login_alert = dbc.Alert(
    'User not logged in. Taking you to login.',
    color='danger'
)

colors = {
    'background': '#FFFFFF',
    'background_2': '#e5f5f2',
    'background_3': '#D0E3F5',
    'text_1': '#74807E',
    'text_2': '#008080', 
    'text_3': '#FF5733',
    'text_4': '#8E7F7A', 
    'text_5': '#193176',
    'text_6': '#45B39D',
    'text_7': '#202a3d',
    'pills_1': '#E5E5E5',
    'conflicting':'#F77910'}

tab_selected_style = {
    'borderTop': '3px solid #E36209 !important'
}



location_conf = dcc.Location(id='page2-url', refresh=True)
expertise_level = ['Novice', 'Advanced Beginner', 'Competent', 'Proficient', 'Expert']
expected_label = ''  # expected label (classification) of variant, to compare with user answer
selected_variant_id_conf = ''  # selected variant id from the main table
df_variants_conf = '' # dataframe con variants de la tabla principal
df_variants_patient = '' # dataframe con pacients de variantes tipo 3

# API requests to be made when the page loads
api_route_variants_conf = '/variants/conf'
status_conf, df_variants_conf = common.api_request(api_route_variants_conf)  # load conflictive variants

api_route_times_clasif = '/variants/times'
status_times_clasif, df_times_clasif = common.api_request(api_route_times_clasif)


method = 'POST'
data = {'is_classif': '1'}
api_route_labels = '/labels'
status_labels, df_labels = common.api_request(api_route_labels, method, data)  # load labels

api_route_comments = '/comments'
status_comments, df_comments = common.api_request(api_route_comments)  # load comments

api_route_rules = '/rules'
status_rules, df_rules = common.api_request(api_route_rules)  # load rules

api_route_sample = '/sample'
status_sample, df_sample  = common.api_request(api_route_sample,'GET')


def layout():
    # API requests to be made when the page loads
    if current_user.access_level == 3:
        categorias = [
            {'label': 'Default', 'value': 1},
            {'label': 'Internas', 'value': 2},
            {'label': 'Pacientes', 'value': 3}]
        value = [3]
    elif current_user.access_level == 2:
        categorias = [
            {'label': 'Default', 'value': 1},
            {'label': 'Internas', 'value': 2}]
        value = [2]
    else:
        categorias = [
            {'label': 'Default', 'value': 1}]
        value = [1]

    return dbc.Row(
        dbc.Col(
            [
                location_conf,
                dbc.Card(
                    dbc.CardBody([
                        html.H5("Etapa 2",className="lead",  style={'color': colors['text_7']}
                        ),
                        html.H2("Etiquetado de variantes", style={'color': colors['text_7']}),
                        html.Hr(className="my-2"),
                        html.Div([
                            dbc.Button(
                                children=html.Span([html.I(className='fas fa-info-circle'), ' Más información']),
                                id = 'open', color="primary"),
                            dbc.Modal([
                                    dbc.ModalHeader("Etiquetado de variantes"),
                                    dbc.ModalBody("Esta página está dedicada al etiquetado de variantes conflictivas."
                                                  "Al usuario se le presentarán variantes de distintos tipos, que tienen en"
                                                  "común a falta de clasificación o un conflicto en la misma. Se sugiere iniciar"
                                                  "este proceso habiendo realizado al menos una vez la instancia de entrenamiento."),
                                    dbc.ModalFooter(
                                        dbc.Button("Close", id="close", className="ml-auto")
                                    ),
                                ],
                                id="modal",
                            )
                        ]),
                    ]), color = "light"
                ),
                html.Br(),
                html.Br(),
                dbc.Row(children=[
                    dbc.Col(dbc.Card(
                        dbc.CardBody(
                            [html.H5("Etiquetado de una variante cargada en la plataforma", className="card-title"),
                             html.Br(),
                             html.P(children='Seleccione el/los tipo/s de variantes que etiquetará:',
                                    style={'fontWeight': 'normal', 'color': colors['text_7']}),
                             dcc.Dropdown(
                                 id='dropdown_selection',
                                 options=categorias,
                                 value=value,
                                 multi=True,
                                 clearable=False,
                                 placeholder="Seleccionar un tipo de variante"
                             ),
                             html.Br(),
                             html.P(children='Información adicional (si corresponde):',
                                    style={'fontWeight': 'normal', 'color': colors['text_7']}),
                             dcc.Dropdown(
                                 id='dropdown_patient',
                                 options=[],
                                 multi=True
                             ),
                             html.Br(),
                             html.Br(),
                             html.Br(),
                             ])
                    )
                        ,width=8),
                    dbc.Col(dbc.Card(
                        dbc.CardBody([
                            html.H5("Si ingresará un archivo con nuevas variantes", className="card-title"),
                            html.Br(),
                            html.Div([dbc.Row([
                                dbc.Col(html.H6("Archivo con variantes"), width=8),
                                dbc.Col(html.Span(id="output-data-upload"), width=4)
                            ])]),
                            dcc.Upload(
                                id='upload-data',
                                children=html.Div([
                                    'Arrastrar y soltar o ',
                                    html.A('Seleccionar archivo')
                                ]),
                                style={
                                    'width': '100%',
                                    'height': '50px',
                                    'lineHeight': '50px',
                                    'borderWidth': '1px',
                                    'borderStyle': 'dashed',
                                    'borderRadius': '5px',
                                    'textAlign': 'center',
                                    #'margin': '10px'
                                },
                                # Allow multiple files to be uploaded
                                multiple=False
                            ),
                            html.Div([
                                html.Span(
                                    [html.I(className='fas fa-download'), " Descargar ejemplo"],
                                    id="button_template"
                                ),
                                Download(id="table_download_template")
                            ]
                            ),
                            html.Br(),
                            html.Div([dbc.Row([
                                dbc.Col(html.H6("Información de la muestra"), width=8),
                                dbc.Col(html.Span(id="output-data-upload_sample"), width=4)
                            ])]),
                            dcc.Upload(
                                id='upload-data_2',
                                children=html.Div([
                                    'Arrastrar y soltar o ',
                                    html.A('Seleccionar archivo')
                                ]),
                                style={
                                    'width': '100%',
                                    'height': '50px',
                                    'lineHeight': '50px',
                                    'borderWidth': '1px',
                                    'borderStyle': 'dashed',
                                    'borderRadius': '5px',
                                    'textAlign': 'center',
                                    # 'margin': '10px'
                                },
                                # Allow multiple files to be uploaded
                                multiple=False
                            ),
                            html.Div([
                                html.Span(
                                    [html.I(className='fas fa-download'), " Descargar ejemplo"],
                                    id="button_template_sample"
                                ),
                                Download(id="table_download_template_sample")
                            ]
                            )
                        ])
                    ), width=4)]
                ),

                html.Br(),
                dbc.Button(children=html.Span([html.I(className="fas fa-play-circle"), ' Comenzar']),
                           id='btn-start', color='primary'),
                html.Div(id='datatable'),
                html.Br(),
                html.Br(),
                html.Br(),
                html.Div(
                    id='details-container-conf',
                    children=[
                        dbc.Card(
                            [#html.H3(id='variant-info-title-conf', children='Variant info', style={'fontWeight': 'normal','color': colors['text_2']}),
                            html.Div(id='variant-info-title-conf', style={"maxHeight": "130px", "overflow": "scroll", 'font-size': '26px','fontWeight': 'normal','color': colors['text_7'
                                                                                                                                                                                 '']})
                            ],
                        body=True, 
                        style={"backgroundColor":colors['background_2']}
                        ),
                        html.Br(),
                        # basic info
                        dbc.Tabs(id="tabs", active_tab="tab_1", children=[
                            dbc.Tab(tab_id="tab_1",label='Información General', children=[
                                html.Br(),
                                html.Br(),   
                                html.H3('Información general', style={'fontWeight': 'normal','color': colors['text_7'],'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                html.Br(),
                                html.Div([
                                    dbc.Row([
                                        dbc.Col([
                                            html.Div(id='classif', style={'margin-bottom':'20px', 'text-align':'center'}),
                                            html.Div(id='tt2')
                                        ], width=1),
                                        dbc.Col([
                                            html.Div(id='acmg', style={'margin-bottom':'20px', 'text-align':'center'}),
                                            html.Div(id='tt')
                                        ], width=2), 
                                        dbc.Col([
                                            html.H6('Quality'),
                                            html.Div(id='quality')
                                        ], width=2),
                                        dbc.Col([
                                            html.H6('DP (Read depth)'),
                                            html.Div(id='dp')
                                        ], width=2)
                                    ])
                                ], style={'text-align':'center'}),

                                html.Br(),
                                dbc.Card(
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-basic-conf1', style={'text-align':'center'}),
                                                html.Div(id='variant-info-basic-conf2', style={'text-align':'center'}),
                                                html.Div(id='variant-info-basic-conf3', style={'text-align':'center'})
                                            ])
                                        ])
                                    ])
                                ),
                                html.Br(),
                                html.Div(
                                    [
                                        dbc.Button(
                                            [html.I(className='fas fa-tag'), " Clasificación previa"],
                                            id="collapse-button",
                                            className="mb-3",
                                            color="primary",
                                        ),
                                        dbc.Collapse(
                                            dbc.Card(dbc.CardBody(html.Div(id = 'coll'))),
                                            id="collapse",
                                        ),
                                    ]
                                ),
                                html.Br(),
                                html.Br(),
                                html.Br()
                            ], tab_style ={'color': colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="tab_2", label='Asociaciones', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Asociaciones',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'],
                                               'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                            dbc.Card([
                                dbc.CardHeader(children=[
                                    dbc.Row([
                                        dbc.Col([
                                            html.Div("OMIM")
                                        ]),
                                        dbc.Col([
                                            html.Div(id='button_omim', style={'textAlign': 'right'})
                                        ])
                                    ])
                                ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '23px'}),
                                dbc.CardBody([
                                    html.Div(id='variant-info-omim-conf'),
                                    # html.H4('OMIM', style={'fontWeight': 'normal','color': colors['text_4'],'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                    html.Br(),
                                    html.H5("Relación Gen-Fenotipo", className="card-title"),
                                    html.Div(id='variant-info-omim-conf_2'),
                                    html.Br()
                                ])
                            ])
                            ], tab_style={"color": colors['text_1'], 'tab-size': '5'}),
                            dbc.Tab(tab_id="tab_3",label='Clasificación online', children=[
                            # asociations
                                html.Br(),
                                html.Br(),
                                html.H3('Clasificación online', style={'fontWeight': 'normal','color': colors['text_7'],
                            'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                html.Br(),
                                # clinvar info
                                dbc.Card([
                                    dbc.CardHeader(children= [
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("ClinVar")
                                            ]),
                                            dbc.Col([    
                                                html.Div(id='button_clinvar', style={'textAlign': 'right'})
                                            ])
                                        ])
                                        ], style={'fontWeight': 'normal','color': colors['text_1'], 'font-size': '23px'}),
                                    dbc.CardBody([
                                        #html.H4('CLINVAR',style={'fontWeight': 'normal','color': colors['text_4'],'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                        
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-clinvar-conf0'),
                                                html.Br(),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-clinvar-conf1', style={'text-align':'center'}),
                                                    ])
                                                ]),
                                                html.Br(),
                                                html.Br(),
                                                html.H5("Interpretaciones realizadas en ClinVar", className="card-title"),
                                                html.Div(id='variant-info-clinvar-conf2', style={'height': '300px', 'overflowY': 'ellipsis','overflow': 'scroll', 'textOverflow': 'ellipsis'}),
                                                html.Br(),
                                                html.H5("Conflictos en interpretaciones", className="card-title"),
                                                html.Div(id='variant-info-clinvar-conf3', style={'height': '200px', 'overflowY': 'ellipsis','overflow': 'scroll', 'textOverflow': 'ellipsis'})
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # OMIM info

                            ], tab_style ={"color": colors['text_1'], 'margin': '0px'}),
                            # frequencies
                            dbc.Tab(tab_id="tab_4",label='Frequencias', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Frequencias poblacionales',style={'fontWeight': 'normal','color': colors['text_7'],
                            'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                dbc.Card(
                                    dbc.CardBody([
                                        html.Br(),
                                        html.Div(id='variant-info-freq-conf1'),
                                        html.Br(),
                                        dbc.Tabs(id="tabs_freq", active_tab="tab_freq_1", children=[
                                            dbc.Tab(tab_id="tab_freq_1", label='Población', children=[
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop-conf1')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop-conf2')
                                                    ])
                                                ]),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq-conf2')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq-conf3')
                                                    ])
                                                ])
                                            ]),
                                            dbc.Tab(tab_id="tab_freq_2", label='Proyecto', children=[
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop-conf3')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop-conf4')
                                                    ])
                                                ]),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq-conf4')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq-conf5')
                                                    ])
                                                ])
                                            ])
                                        ]),
                                        html.Div(children=html.Span([html.I(className='fas fa-circle', style={'color':px.colors.qualitative.Set1[0]}), ' f<0.1%  ',
                                                                     html.I(className='fas fa-circle', style={'color':'#fcc323'}), '  0.1%<f<1%  ',
                                                                     html.I(className='fas fa-circle', style={'color':px.colors.qualitative.Vivid[2]}), '  1%<f<5%  ',
                                                                     html.I(className='fas fa-circle', style={'color':px.colors.qualitative.Vivid[5]}), ' f>5%  ']))
                                    ])
                                ),
                                # scores
                                html.Br()
                            ], tab_style ={"color": colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="5",label='Predictores', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Predictores', style={'fontWeight': 'normal','color': colors['text_7'],
                                'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                # Predictores impacto fisicoquímico
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Impacto Fisicoquímico")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-fq-scores-conf1')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # Predictores conservación evolutiva
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Conservación Evolutiva")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-evo-scores-conf1')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # Predictores splicing
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Splicing")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-splicing-scores-conf1')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br()
                            ], tab_style ={"color": colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="tab_6",label='Reglas ACMG', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Clasificación ACMG', style={'fontWeight': 'normal', 'color': colors['text_7'],
                                                              'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                dbc.Card(children=[
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='acmg_0')
                                            ]),
                                            dbc.Col([
                                                html.Div(id='button_acmg', style={'textAlign': 'right'})
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '23px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='ACMG_1', style={'text-align': 'center'}),
                                                #html.Div(id='variant-info-basic-conf2', style={'text-align': 'center'}),
                                                #html.Div(id='variant-info-basic-conf3', style={'text-align': 'center'})
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                html.Div(
                                    [
                                        dbc.Button(
                                            [html.I(className='fas fa-book'), " Referencias"],
                                            id="collapse-button_2",
                                            className="mb-3",
                                            color="primary",
                                        ),
                                        dbc.Collapse(
                                            dbc.Card(dbc.CardBody(
                                                html.Div(id='coll_2'
                                                         ))),
                                            id="collapse_2",

                                        ),
                                    ]
                                ),
                                html.Br(),

                            ], tab_style ={"color": colors['text_1'],'margin': '0px'}),
                            dbc.Tab(tab_id="tab_8", label='Muestra', children=[
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.H3('Información de la muestra', style={'fontWeight': 'normal', 'color': colors['text_7'],
                                                                   'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                dbc.Card(
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-sample-conf1', style={'text-align': 'center'}),
                                                html.Div(id='variant-info-sample-conf2', style={'text-align': 'center'}),
                                            ])
                                        ])
                                    ])
                                ),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                            ], tab_style ={"color": colors['text_1'],'margin': '0px'}),
                            dbc.Tab(tab_id="tab_7",label='Etiquetado', children=[
                                html.Br(),
                                html.Br(),
                                # user classification
                                html.Br(),
                                html.Br(),
                                # user classification
                                html.H3('Su clasificación', style={'fontWeight': 'normal', 'color': colors['text_7'],
                                                                   'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                html.H6(
                                    children=html.Span([html.I(className='fas fa-tag'),
                                                        ' De acuerdo con la información proporcionada, establezca una clasificación para la variante que está viendo actualmente. Corresponde a:'])),
                                dcc.Dropdown(
                                    id='combo-labels2',
                                    value='',
                                    placeholder= "Seleccionar la patogenicidad para la variante..."
                                ),
                                html.Br(),
                                dcc.Dropdown(
                                    id='combo-comments',
                                    value='',
                                    placeholder= "Información adicional sobre la variante..."
                                ),
                                html.Br(),
                                dbc.Button(
                                    children=html.Span([html.I(className='fas fa-paper-plane'), ' Enviar clasificación']),
                                    id='btn-submit-classification2', n_clicks=0, color='primary'),
                                html.Br(),
                                html.Br(),
                                dbc.Alert(id='msg-classification-feedback2', children='', color='danger',
                                          style={'display': 'none'}),
                                html.Br(),
                                html.Div([
                                    dbc.Button(
                                        [html.I(className='fas fa-download'), " Descargar variante"],
                                        id="btn",
                                        className="mb-3",
                                        color="info",
                                    ),
                                    Download(id="download")
                                ]
                                ),
                                html.Br(),
                                html.Br(),

                            ], tab_style ={'color': colors['text_1']}),
                        ], style ={'font-size': '17px'})
                    ],
                    style={'display': 'none'},
                ),
            ], width=12)
    )
    # if current_user.is_authenticated:



@app.callback(
    Output("modal", "is_open"),
    [Input("open", "n_clicks"), Input("close", "n_clicks")],
    [State("modal", "is_open")],
)
def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

@app.callback(
    Output("collapse_2", "is_open"),
    [Input("collapse-button_2", "n_clicks")],
    [State("collapse_2", "is_open")],
)
def toggle_collapse_2(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse", "is_open"),
    [Input("collapse-button", "n_clicks")],
    [State("collapse", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("collapse-download", "is_open"),
    [Input("collapse-button-download", "n_clicks")],
    [State("collapse-download", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output('table_download_template', 'data'),
    [Input('button_template', 'n_clicks')]
)
def download(n_clicks):
    context_id = common.get_trigger_id()
    print('element id:', context_id)
    global df_variants_conf_template
    if context_id == 'button_template':
        method = 'GET'
        api_route_variants_conf = '/variants/999456'
        status_conf_template, df_variants_conf_template = common.api_request(api_route_variants_conf,method)

    return send_data_frame(df_variants_conf_template.drop(columns=["count(*)"]).to_csv, "variant_template.tsv", index=False, sep='\t')

@app.callback(
    Output('table_download_template_sample', 'data'),
    [Input('button_template_sample', 'n_clicks')]
)
def download(n_clicks):
    context_id = common.get_trigger_id()
    if context_id == 'button_template_sample':
        method = 'GET'
        api_route_sample = '/sample/ERM'
        status_sample_template, df_sample_template  = common.api_request(api_route_sample,method)
    return send_data_frame(df_sample_template.to_csv, "sample_template.tsv", index=False, sep='\t')

@app.callback(
    [Output('output-data-upload', 'children')],
    [Input('upload-data', 'contents')],
    [State('upload-data', 'filename')]
)
def update_output(list_of_contents, list_of_names):
    if list_of_contents is not None:
        df= common.parse_contents(list_of_contents, list_of_names)
        uploaded = [html.I(className='fas fa-check', style = {'color':'#7BC6BE'})]
        return uploaded
    else:
        uploaded = [html.I(className='fas fa-xmark', style = {'color':'#FD6651'})]
        return uploaded

@app.callback(
    [Output('output-data-upload_sample', 'children')],
    [Input('upload-data_2', 'contents')],
    [State('upload-data_2', 'filename')]
)
def update_output(list_of_contents_sample, list_of_names_sample):
    if list_of_contents_sample is not None:
        df_2 = common.parse_contents(list_of_contents_sample, list_of_names_sample)
        print(df_2)
        uploaded = [html.I(className='fas fa-check', style={'color': '#7BC6BE'})]
        return uploaded
    else:
        uploaded = [html.I(className='fas fa-xmark', style={'color': '#FD6651'})]
        return uploaded






# Populate the options of patients dropdown based on type of variant option (if type 3 is taken)
@app.callback(
    Output('dropdown_patient', 'options'),
    [Input('dropdown_selection', 'value')]
)
def set_patient_options(chosen_variant_type):
    api_route_variants_conf = '/variants/conf/patient'
    data = {'user': current_user.user,
            'selection': str(chosen_variant_type)}  # selection has to be a string to avoid problems with API
    global df_variants_patient
    status_conf, df_variants_patient = common.api_request(api_route_variants_conf, 'POST',
                                                       data)
    if (chosen_variant_type == [3]):
        return [{'label': c, 'value': c} for c in df_variants_patient['patient'].unique()]
    else:
        return []

# populate initial values of patients dropdown
@app.callback(
    Output('dropdown_patient', 'value'),
    [Input('dropdown_patient', 'options')]
)
def set_patient_value(available_patients):
    return [x['value'] for x in available_patients]


@app.callback(
    [Output("datatable", "children")],
    [Input("btn-start", "n_clicks")],
    [State("dropdown_selection", "value"),
    State("dropdown_patient", "value")]
)
def show_table(n_clicks_start, val_chosen, val_chosen_2):
    context_id = common.get_trigger_id()
    print('element id:', context_id)
    if context_id == 'btn-start':
        api_route_variants_conf = '/variants/conf/part'
        data = {'user': current_user.user, 'selection': str(val_chosen), 'selection_2': str(val_chosen_2)}  #selection has to be a string to avoid problems with API
        global df_variants_conf
        status_conf, df_variants_conf = common.api_request(api_route_variants_conf, 'POST',
                                                           data)  # load conflictive variants
        df_variants_conf = df_variants_conf.rename(columns={'AAChange.refGene': 'Cambio AA', 'Func.refGene': 'Región', 'ExonicFunc.refGene':'Función exónica', 'Gene.refGene': 'Gen', 'avsnp150':'rsID', 'freq.max':'Frec. max'}).round(4)
        table_select = html.Div(children=[
            html.Br(),
            html.Br(),
            html.P(children='Por favor, seleccione una variante de la lista:',
                   style={'fontWeight': 'normal', 'color': colors['text_7']}),
            dash_table.DataTable(
            id='datatable-interactivity2',
            columns=[
                {"name": i, "id": i, "deletable": False, "selectable": True} \
                for i in df_variants_conf.loc[:, df_variants_conf.columns != 'ID'].columns
            ],
            data=df_variants_conf.to_dict('records'),
            # editable=True,
            filter_action="native",
            sort_action="native",
            sort_mode="multi",
            # column_selectable="single",
            row_selectable="single",
            row_deletable=False,
            selected_columns=[],
            selected_rows=[],
            page_action="native",
            page_current=0,
            page_size=7,
            # style_table={'overflowX': 'scroll'},
            style_cell={
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
                'maxWidth': '250px',
                'textAlign': 'center',
                'color': colors['text_7'],

            },
            style_data={
                'font-family': "Open sans light, sans-serif",
                'font_size': '14px',
            },
            # tooltip_data=[
            #     {
            #         column: {'value': str(value), 'type': 'markdown'}
            #         for column, value in row.items()
            #     } for row in df_variants_conf.to_dict('rows')
            # ],
            # tooltip_duration=None,
            style_header={
                'backgroundColor': 'rgb(235, 235, 235)',
                'fontWeight': 'bold',
                'font-family': "Open-sans, sans-serif"
            },
            # tooltip_data=[
            #   {
            #      column: {'value': str(value), 'type': 'markdown'}
            #     for column, value in row.items()
            # } for row in df_variants_conf.to_dict('records')
            # ],
            # css=[{
            #    'selector': '.dash-table-tooltip',
            #    'rule': 'placement: auto'
            # }],
            # tooltip_duration=None,
            # tooltip_delay=0,
            style_as_list_view=True,
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }
            ],
            # style_as_list_view=True,
        ),
        html.Br(),
        dbc.Button(children=html.Span([html.I(className='fas fa-eye'), ' Ver detalles']), id='btn-view-details2',
                   n_clicks=0, color='primary'),
        html.Br(),
        html.Br(),
        html.P(children='O tome una variante conflictiva de forma aleatoria', style={'color': colors['text_7']}),
        html.Br(),
        dbc.Button(children=html.Span([html.I(className='fas fa-random'), ' Seleción aleatoria']),
                       id='btn-random-variant2',
                       n_clicks=0, color='primary'),
        html.Br(),
        ])

        #html.Br()

        return [table_select]


@app.callback(
    [
        Output('variant-info-title-conf', 'children'),
        #Output('transcripts', 'children'),
        Output('classif', 'children'),
        Output('tt2', 'children'),
        Output('acmg', 'children'),
        Output('tt', 'children'),
        Output('quality', 'children'),
        Output('dp', 'children'),
        Output('variant-info-basic-conf1', 'children'),
        Output('variant-info-basic-conf2', 'children'),
        Output('variant-info-basic-conf3', 'children'),
        Output('coll', 'children'),
        Output('button_clinvar', 'children'),
        Output('variant-info-clinvar-conf0', 'children'),
        Output('variant-info-clinvar-conf1', 'children'),
        Output('variant-info-clinvar-conf2', 'children'),
        Output('variant-info-clinvar-conf3', 'children'),
        Output('button_omim', 'children'),
        Output('variant-info-omim-conf', 'children'),
        Output('variant-info-omim-conf_2', 'children'),
        Output('variant-info-freq-conf1', 'children'),
        Output('variant-info-pop-conf1', 'children'),
        Output('variant-info-pop-conf2', 'children'),
        Output('variant-info-freq-conf2', 'children'),
        Output('variant-info-freq-conf3', 'children'),
        Output('variant-info-pop-conf3', 'children'),
        Output('variant-info-pop-conf4', 'children'),
        Output('variant-info-freq-conf4', 'children'),
        Output('variant-info-freq-conf5', 'children'),
        Output('variant-info-fq-scores-conf1', 'children'),
        Output('variant-info-evo-scores-conf1', 'children'),
        Output('variant-info-splicing-scores-conf1', 'children'),
        Output('acmg_0', 'children'),
        Output('button_acmg', 'children'),
        Output('ACMG_1', 'children'),
        Output('coll_2', 'children'),
        Output('variant-info-sample-conf1', 'children'),
        Output('variant-info-sample-conf2', 'children'),
        Output('combo-labels2', 'options'),
        Output('combo-labels2', 'value'),  # allows to reset the dropdown to the default value
        Output('combo-comments', 'options'),
        Output('combo-comments', 'value'),  # allows to reset the dropdown to the default value
        Output('tabs', 'active_tab'),
        Output('tabs_freq', 'active_tab'),
        Output('table_download', 'children'),
        Output('details-container-conf', 'style'),
        # Output('msg-classification-feedback', 'style'),
        # Output('btn-submit-classification2', 'disabled')
    ],
    [

        Input('datatable-interactivity2', 'selected_rows'),
        Input('btn-view-details2', 'n_clicks'),
        Input('btn-random-variant2', 'n_clicks'),
        Input("dropdown_selection", "value"),
        Input("dropdown_patient", "value")
        # Input('btn-submit-classification2', 'n_clicks'),
        # Input('combo-labels2', 'value')
    ]
)

def view_variant(sel_rows, n_clicks_view, n_clicks_random, val_chosen, val_chosen_2):
    """
    Load and display information of the selected variant
    :param sel_rows: selected rows indices
    :param n_clicks_view: n clicks in the 'show info' button
    :param n_clicks_random: n clicks in the 'random' button
    :return: [title, table, table, table, style]
    """

    context_id = common.get_trigger_id()
    #print('element id:', context_id)

    if (len(sel_rows) == 1 and context_id == 'btn-view-details2') or (context_id == 'btn-random-variant2'):
        # avoid errors when no row is selected, by checking length of sel_rows
        global selected_variant_id_conf
        global df_variants_conf
        route = '/variants'
        method = 'GET'

        if context_id == 'btn-random-variant2':
            # request a random variant to API
            # data = {'variant_id': -1}  # set id to -1 to request a random variant
            route = route + '/conf/random'
            data = {'user': current_user.user, 'selection': str(val_chosen)}
            status_conf, df_selected_variant = common.api_request(route, 'POST', data)

            # print(df_selected_variant)

            # get the ID of the random variant
            selected_variant_id_conf = df_selected_variant.at[0, 'ID']
            # get the variant AA change
            selected_variant_AAchange = df_selected_variant.at[0, 'AAChange.refGene']
        else:
            # get variant id from initially loaded dataframe
            selected_variant_id_conf = df_variants_conf.at[sel_rows[0], 'ID']
            # get the variant AA change from initially loades dataframe
            selected_variant_AAchange = df_variants_conf.at[sel_rows[0], 'Cambio AA']

            # request variant data to API
            # data = {'variant_id': int(selected_variant_id_conf)}
            route = route + '/' + str(selected_variant_id_conf)
            status_conf, df_selected_variant = common.api_request(route, method, True)
            # print(df_selected_variant)

        # prepare variant info to display
        print('\nSelected variant:', selected_variant_AAchange)
        global expected_label
        expected_label = df_selected_variant.at[0, 'CLNSIG']

        times_route = '/variants/times/' + str(selected_variant_id_conf)
        status_times_clasif, df_times_clasif = common.api_request(times_route, method)

        conflict_route = '/variants/conflict/' + str(selected_variant_id_conf)
        status_conflict_info, df_conflict_info = common.api_request(conflict_route, method)

        clinvar_submission_route = '/variants/clin_submission/' + str(selected_variant_id_conf)
        status_clinvar_submission, df_clinvar_submission = common.api_request(clinvar_submission_route, method)

        # prepare variant info to display
        if pd.isnull(selected_variant_AAchange):
            print('is null')
            title = 'Variante a etiquetar: ' + str(selected_variant_AAchange)
        else:
            spl = selected_variant_AAchange.split(",")
            spl = DataFrame(spl,columns=['Variante a etiquetar: '])
            title = dbc.Table.from_dataframe(spl, borderless =True, style={'font-size': '26px','fontWeight': 'normal','color': colors['text_7']})

        #title = 'Info of variant: ' + str(selected_variant_AAchange)
        #title = dbc.Table.from_dataframe(spl, borderless =True, style={'font-size': '26px','fontWeight': 'normal','color': colors['text_2']})

        #####################################################################################################
        # Basic information
        #####################################################################################################
        
        #if (df_selected_variant[['QUAL']].values < 25).bool:
           # bar_color="danger"
        #if ((df_selected_variant[['QUAL']].values > 25) & (df_selected_variant[['QUAL']].values < 75)).bool:
          #  bar_color="warning"
        #else:
         #   bar_color="success"


        acmg = ["BRCA1", "BRCA2", "TP53", "STK11", "MLH1", "MSH2", "MSH6", "PMS2", "APC", "MUTYH", "VHL", "MEN1", "RET", "PTEN", "RB1", "SDHD", "SDHAF2", "SDHC", "SDHB", "TSC1", "TSC2", "WT1", "NF2", "COL3A1", "FBN1", "TGFBR1", "TGFBR2", "ACTA2", "MYH11", "MYBPC3", "MYH7", "TNNT2", "TNNI3", "TPM1", "MYL3", "ACTC1", "PRKAG2", "GLA", "MYL2", "LMNA", "RYR2", "PKP2", "DSP", "DSC2", "TMEM43", "DSG2", "KCNQ1", "KCNH2", "SCN5A", "LDLR", "APOB", "PCSK9", "RYR1", "CACNA1S", "PALB2", "MAX", "TMEM127", "BMPR1A", "SMAD4", "SMAD3", "CASQ2", "TRDN", "FLNC", "TTN", "BDT", "OTC", "GAA", "HFE", "ACVRL1", "ENG", "HNF1A", "RPE65", "ATP7B"]
        if df_selected_variant['Gene.refGene'].values in acmg:
            is_acmg = html.H4(dbc.Badge("ACMG", color="info", className="lml-1", pill=True, id = "nacmg"))
        else:
            is_acmg = html.H4(dbc.Badge("ACMG", className="lml-1", pill=True, id = "nacmg", style={'background-color':  colors['pills_1']}))

        tt = dbc.Tooltip("Si esta etiqueta está coloreada, indica que la variante está en un gen de relevancia preventiva.", target="nacmg")

        if df_selected_variant['count(*)'].values > 0:
            tag = html.H4(dbc.Badge(children=html.Span([html.I(className='fas fa-tag'), " Clasificado"]), color="info", className="lml-1", pill=True, id="was_classif"))
        else:
            tag = html.H4(dbc.Badge(children=html.Span([html.I(className='fas fa-tag'), " Clasificado"]), style={'background-color': colors['pills_1']}, className="lml-1", pill=True, id="was_classif"))

        tt2 = dbc.Tooltip("Si esta etiqueta está coloreada, indica que la variante ya fue clasificada previamente.", target="was_classif")


        qual = dbc.Progress(value=df_selected_variant['QUAL'])
        dp = dbc.Progress(value=df_selected_variant['DP'])

        basic_info = df_selected_variant[['Chr', 'cytoBand','Start', 'End', 'Ref', 'Alt']]
        basic_info = basic_info.rename(columns={'Chr':'Cromosoma', 'cytoBand':'Citobanda', 'Ref':'REF', 'Alt':'ALT'})
        basic_table = dbc.Table.from_dataframe(basic_info)
        
        #basic_info2 =  df_selected_variant.iloc[:, 10:13]
        basic_info2 = df_selected_variant[['Assembly', 'Gene.refGene', 'Func.refGene', 'ExonicFunc.refGene']]
        basic_info2 = basic_info2.rename(columns={'Gene.refGene': 'Gen', 'Func.refGene': 'Región', 'ExonicFunc.refGene': 'Función exónica'})
        basic_table2 = dbc.Table.from_dataframe(basic_info2)
        #striped=True, bordered=True, hover=True

        basic_info3 = df_selected_variant[['type_2', 'snp138', 'MC']].replace("_", " ", regex=True)
        basic_info3 = basic_info3.rename(columns={'type_2': 'Tipo de variante', 'snp138': 'RS ID (dbSNP 138)', 'MC': 'Consecuencia Molecular'})
        basic_table3 = dbc.Table.from_dataframe(basic_info3)

        times_clasif = df_times_clasif.rename(columns = {'user_ID': 'Usuario', 'level': 'Nivel','''DATE_FORMAT(timestamp, '%d/%m/%Y %H:%i:%S')''':'Fecha','label_ID':'Etiqueta', 'COMMENT':'Commentario'})
        coll = dbc.Table.from_dataframe(times_clasif)


        #####################################################################################################
        # Associations info
        #####################################################################################################

        #####################################################################################################
        #    ClinVar
        #####################################################################################################

        #butt_clin = dbc.Button("View record on ClinVar", className="ml-auto",href="https://www.ncbi.nlm.nih.gov/clinvar/variation/", external_link=True)
        var_id = str(df_selected_variant['VariationID'].values).strip('[]')
        button_clinvar= html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Ver entrada en ClinVar"]), href=config.clinvar_base_url+var_id, target="_blank", style={"font-size": "14px"})
        
        variant_name = str(df_selected_variant['Name'].values).strip("[']")
        name_clinvar = html.H5(children=["Variant name: ", html.A(variant_name, style={'color': colors['text_6'], 'font-weight':'bold'})], style={'color': colors['text_6']})

        
        clinvar_info1 = df_selected_variant[['CLNSIG', 'CLNSIGCONF']].replace("_", " ", regex=True)
        clinvar_info1 = clinvar_info1.rename(columns={'CLNSIG':'Significado Clínico'})
        #clinvar_table1 = dbc.Table.from_dataframe(clinvar_info1, borderless =True, style = {'color':'orange'})


        clinvar_info = df_selected_variant[['CLNREVSTAT','LastEvaluated','NumberSubmitters']].replace("_", " ", regex=True)
        #clinvar_info = clinvar_info.replace("_", " ", regex=True)
        clinvar_info = clinvar_info.rename(columns={'CLNREVSTAT':'Status de revisión', 'LastEvaluated':'Última evaluación', 'NumberSubmitters':'Número de remitentes'})
        #clinvar_table = dbc.Table.from_dataframe(clinvar_info, borderless =True)

        if clinvar_info[clinvar_info.columns.values[0]].values == 'criteria provided, conflicting interpretations' or \
                clinvar_info[clinvar_info.columns.values[0]].values == 'criteria provided, single submitter':
            stars = 1
        elif clinvar_info[clinvar_info.columns.values[0]].values == 'practice guideline':
            stars = 4
        elif clinvar_info[clinvar_info.columns.values[0]].values == 'reviewed by expert panel':
            stars = 3
        elif clinvar_info[
            clinvar_info.columns.values[0]].values == 'criteria provided, multiple submitters, no conflicts':
            stars = 2
        else:
            stars = 0

        stars_list = ['(']
        # filled stars
        for s in range(1, stars + 1):
            stars_list.append(html.I(className='fas fa-star'))
        # blank stars
        for t in range(1, 4 - stars + 1):
            stars_list.append(html.I(className='far fa-star'))
        stars_list.append(')')
        star = html.Span(stars_list)

        #Clinvar general information visualization
        # todo: mejorar la forma de mostrar esta parte
        clinvar_table = html.Div(
                            [dbc.Row(
                                [dbc.Col(
                                    html.Div([
                                        dbc.Row(children = clinvar_info1.columns.values[0],
                                                style = {'fontWeight': 'bold'},
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children = clinvar_info1[clinvar_info1.columns.values[0]].values,
                                                style={'color': colors['conflicting'],'fontWeight': 'bold'},
                                                justify="center", align="center"
                                        ),
                                        dbc.Row(children=clinvar_info1[clinvar_info1.columns.values[1]].values,
                                                justify="center", align="center"
                                        )
                                    ]), width=4
                                ),
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children = clinvar_info.columns.values[0],
                                                style = {'fontWeight': 'bold'},
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children = clinvar_info[clinvar_info.columns.values[0]].values,
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children=star,
                                                justify="center", align="center"
                                                )
                                    ])
                                ),
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children = clinvar_info.columns.values[1],
                                                style={'fontWeight': 'bold'},
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children = clinvar_info[clinvar_info.columns.values[1]].values,
                                                justify = "center", align = "center"
                                        )
                                    ])
                                ),
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children = clinvar_info.columns.values[2],
                                                style={'fontWeight': 'bold'},
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children = clinvar_info[clinvar_info.columns.values[2]].values,
                                                justify = "center", align = "center"
                                        )
                                    ])
                                )
                                ]
                            ),
                            html.Br(),
                            dbc.Row([
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children = basic_info3.columns.values[0],
                                                style={'fontWeight': 'bold'},
                                                justify = "center", align = "center"
                                        ),
                                        dbc.Row(children = basic_info3[basic_info3.columns.values[0]].values,
                                                justify = "center", align = "center"
                                        )
                                    ])
                                ),
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children='ID de NCBI',
                                                style={'fontWeight': 'bold'},
                                                justify="center", align="center"
                                                ),
                                        dbc.Row(children=var_id,
                                                justify="center", align="center"
                                                )
                                    ])
                                ),
                                dbc.Col(
                                    html.Div([
                                        dbc.Row(children='ID de Alelo (ClinVar)',
                                                style={'fontWeight': 'bold'},
                                                justify="center", align="center"
                                                ),
                                        dbc.Row(children=df_selected_variant[['CLNALLELEID']][df_selected_variant[['CLNALLELEID']].columns.values[0]].values,
                                                justify="center", align="center"
                                                )
                                    ])
                                )
                            ])
                        ])

        #clinvar_info2 = df_selected_variant['PhenotypeList'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'Phenotype'})
        #clinvar_table2 = dbc.Table.from_dataframe(clinvar_info2, striped=True, bordered=True, hover=True)

        #clinvar_info4 = df_selected_variant['PhenotypeIDS'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'References'}).replace(";", "; ", regex=True).replace(",", ", ", regex=True)
        #clinvar_table4 = dbc.Table.from_dataframe(clinvar_info4, striped=True, bordered=True, hover=True)

        #clinvar_info5 = df_selected_variant['RCVaccession'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'RCV accession'})
        # clin = pd.concat([clinvar_info2, clinvar_info4, clinvar_info5], axis=1)

        clinvar_submissions = df_clinvar_submission['ReportedPhenotypeInfo'].str.split(":", n=1, expand=True)
        clinvar_sub = [0] * len(clinvar_submissions[0])
        for i in range(len(clinvar_submissions[0])):
            clinvar_sub[i] = [html.A(clinvar_submissions[1][i], href="https://www.ncbi.nlm.nih.gov/medgen/"+clinvar_submissions[0][i], target="_blank")]

        clinvar_sub = DataFrame(clinvar_sub, columns=['Reported Phenotype'])
        submitter = df_clinvar_submission[['ClinicalSignificance', 'ReviewStatus', 'Submitter','DateLastEvaluated', 'CollectionMethod',
                                           'OriginCounts', 'Description']].rename(columns = {'ClinicalSignificance':'Significado Clínico',
                                                                              'ReviewStatus':'Estado de revisión',
                                                                              'DateLastEvaluated':'Última evaluación',
                                                                              'CollectionMethod':'Método de colección',
                                                                              'OriginCounts':'Origen', 'Description':'Detalles de evidencia'})
        clin = pd.concat([clinvar_sub, submitter], axis = 1)
        clinvar_table5 = dbc.Table.from_dataframe(clin , striped=True, bordered=True, hover=True, style={'overflow': 'ellipsis', 'textOverflow': 'ellipsis'})


        if df_selected_variant['CLNSIG'].values == 'Uncertain_significance':
            clinvar_info6 = df_conflict_info[[]]
        else:
            clinvar_info6 = df_conflict_info[['Submitter1', 'Submitter1_SCV', 'Submitter1_ClinSig', 'Submitter1_ReviewStatus','Submitter1_Sub_Condition','Submitter2', 'Submitter2_SCV', 'Submitter2_ClinSig','Submitter2_ReviewStatus', 'Submitter2_Sub_Condition','Rank_diff', 'Submitter1_Method','Submitter2_Method']]


        #clinvar_info6 = df_conflict_info.iloc[:, np.r_[3:18, 20:22]]
        clinvar_table6 = dbc.Table.from_dataframe(clinvar_info6, striped=True, bordered=True, hover=True, size = 'small',  style={'height': '10px', 'overflowY': 'scroll', 'overflow': 'scroll', 'textOverflow': 'ellipsis'})




        #####################################################################################################
        #    OMIM
        #####################################################################################################

        mim_number = str(df_selected_variant['MIM'].values).strip('[]')
        button_omim= html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Ver entrada del Gen en OMIM"]), href=config.omim_base_url+mim_number, target="_blank", style={"font-size": "14px"})

        omim_info = str(df_selected_variant['MIM'].values).strip('[]')
        omim_table = html.H5(children=["Número MIM de Gen/Locus: ", html.A(omim_info, href=config.omim_base_url+mim_number, target="_blank")], style={'color': colors['text_7']})

        if df_selected_variant['Phenotypes'].empty == False:
            omim_info_2 = df_selected_variant['Phenotypes'].str.split(";", expand=True).stack().to_frame().rename(columns={0:'Phenotype, Phenotype MIM number, Phenotype mapping key, Inheritance'})

            omim = pd.DataFrame()

            omim[['Fenotipo', 'Other']] = omim_info_2['Phenotype, Phenotype MIM number, Phenotype mapping key, Inheritance'].str.split(", (?=\d{3,}|}|])", expand=True)
            omim[['Número MIM Fenotipo','Other2']]= omim['Other'].str.split(" (?=\(\d\))",expand=True)
            omim[['Mapping key del fenotipo', 'Modo de herencia']] = omim['Other2'].str.split("\),", expand=True)
            omim['Mapping key del fenotipo'] = omim['Mapping key del fenotipo'].str.replace('[\(\)]', '')
            omim_final= omim[['Fenotipo','Número MIM Fenotipo','Mapping key del fenotipo','Modo de herencia']]

            # TODO: the tooltip has to correspond the mapping key conditionally
            for i in range(len(omim_final['Mapping key del fenotipo'])):
                omim_final['Mapping key del fenotipo'][0,i] = html.Div(
                    [html.A(html.Span([
                        omim_final['Mapping key del fenotipo'][0,i]], id="tooltip-key",style={"textDecoration": "underline", "cursor": "pointer"})),
                        dbc.Tooltip([html.P("(1): El trastorno se posicionó mediante el mapeo del gen de tipo salvaje."),
                                     html.P("(2): El fenotipo de la enfermedad en sí fue mapeado."),
                                     html.P("(3): Se conoce la base molecular del trastorno."),
                                     html.P("(4): El trastorno es un síndrome de deleción o duplicación cromosómica")
                                     ],
                                    target="tooltip-key"
                                    )
                    ])

            # TODO: the tooltip has appear only when the case appears
            for i in range(len(omim_final['Fenotipo'])):
                omim_final['Fenotipo'][0,i] = html.Div(
                    [html.A(html.Span([
                        omim_final['Fenotipo'][0,i]], id="tooltip-target",style={"textDecoration": "underline", "cursor": "pointer"})),
                        dbc.Tooltip([html.P("Los corchetes, '[ ]', indican 'no enfermedad', principalmente variaciones genéticas que dan lugar a valores de pruebas de laboratorio aparentemente anormales."),
                                     html.P("Las llaves, '{ }', indican mutaciones que contribuyen a la susceptibilidad a trastornos multifactoriales o a la susceptibilidad a infecciones."),
                                     html.P("Un signo de interrogación, '?', delante del nombre del fenotipo indica que la relación entre el fenotipo y el gen es provisional.")],
                                    target="tooltip-target"
                                    )
                    ])

            for i in range(len(omim_final['Número MIM Fenotipo'])):
                omim_final['Número MIM Fenotipo'][0,i] = [html.A(html.Span([omim_final['Número MIM Fenotipo'][0,i]," ",html.I(className='fas fa-external-link-alt')]), href=config.omim_base_url+omim_final['Número MIM Fenotipo'][0,i], target="_blank")]
            omim_table_2 = dbc.Table.from_dataframe(omim_final, striped=True, bordered=True, hover=True)
        else:
            omim_table_2 = []



        #####################################################################################################
        # Frequencies info
        #####################################################################################################

        # Show maximum frequency
        freq_m = round(df_selected_variant['freq.max'].item(), 6)
        if freq_m <= 0.001:
            col_f = px.colors.qualitative.Set1[0]
        elif 0.001 < freq_m <= 0.01:
            col_f = '#fcc323'
        elif 0.01 < freq_m <= 0.05:
            col_f = px.colors.qualitative.Vivid[2]
        elif freq_m > 0.05:
            col_f = px.colors.qualitative.Vivid[5]
        else:
            col_f = 'black'

        freq_max = html.H5(children=["Frecuencia máxima: ", html.A(freq_m, style = {'color':col_f})], style ={'font-style':'italic'})

        #Population Frequencies

        #European
        freq_eur = df_selected_variant[['1000g2015aug_eur', 'ExAC_FIN', 'ExAC_NFE', 'gnomAD_genome_AF_fin','gnomAD_genome_AF_nfe', 'esp6500siv2_ea']].rename(columns={'1000g2015aug_eur':'1000g_eur', 'gnomAD_genome_AF_fin':'gnomAD_fin', 'gnomAD_genome_AF_nfe':'gnomAD_nfe'})
        #African
        freq_afr = df_selected_variant[['1000g2015aug_afr', 'ExAC_AFR', 'gnomAD_genome_AF_afr', 'esp6500siv2_aal']].rename(columns={'1000g2015aug_afr':'1000g_afr', 'gnomAD_genome_AF_afr':'gnomAD_afr', 'esp6500siv2_aal':'esp6500siv2_aa'})
        #Amerindian
        freq_amr = df_selected_variant[['1000g2015aug_amr', 'ExAC_AMR', 'gnomAD_genome_AF_amr']].rename(columns={'gnomAD_genome_AF_amr':'gnomAD_amr', '1000g2015aug_amr':'1000g_amr'})
        #Asian
        freq_as = df_selected_variant[['1000g2015aug_eas', '1000g2015aug_sas', 'ExAC_EAS', 'ExAC_SAS', 'gnomAD_genome_AF_eas', 'gnomAD_genome_AF_sas']].rename(columns={'1000g2015aug_eas':'1000g_eas', '1000g2015aug_sas':'1000g_sas', 'gnomAD_genome_AF_eas':'gnomAD_eas', 'gnomAD_genome_AF_sas':'gnomAD_sas'})
        #Other
        freq_oth = df_selected_variant[['ExAC_OTH', 'gnomAD_genome_AF_oth']]

        # Project Frequencies

        # 1000 genomes
        freq_1000g = df_selected_variant[['1000g2015aug_all', '1000g2015aug_eur', '1000g2015aug_afr', '1000g2015aug_amr','1000g2015aug_eas','1000g2015aug_sas']].rename(columns={'1000g2015aug_all':'1000g_all', '1000g2015aug_eur':'1000g_eur', '1000g2015aug_afr':'1000g_afr', '1000g2015aug_eas':'1000g_eas', '1000g2015aug_sas':'1000g_sas','1000g2015aug_amr':'1000g_amr'})
        # ExAC
        freq_exac = df_selected_variant[['ExAC_ALL','ExAC_FIN', 'ExAC_NFE', 'ExAC_AFR', 'ExAC_AMR','ExAC_EAS', 'ExAC_SAS', 'ExAC_OTH']]
        # gnomAD
        freq_gnomad = df_selected_variant[['gnomAD_genome_AF', 'gnomAD_genome_AF_fin', 'gnomAD_genome_AF_nfe', 'gnomAD_genome_AF_afr',
                                           'gnomAD_genome_AF_amr', 'gnomAD_genome_AF_eas', 'gnomAD_genome_AF_sas',
                                           'gnomAD_genome_AF_oth']].rename(columns={'gnomAD_genome_AF':'gnomAD',
                                                                                    'gnomAD_genome_AF_afr':'gnomAD_afr',
                                                                                    'gnomAD_genome_AF_fin':'gnomAD_fin',
                                                                                    'gnomAD_genome_AF_nfe':'gnomAD_nfe',
                                                                                    'gnomAD_genome_AF_amr':'gnomAD_amr',
                                                                                    'gnomAD_genome_AF_eas':'gnomAD_eas',
                                                                                    'gnomAD_genome_AF_sas':'gnomAD_sas',
                                                                                    'gnomAD_genome_AF_oth':'gnomAD_oth'})

        freq_esp = df_selected_variant[['esp6500siv2_all','esp6500siv2_aal','esp6500siv2_ea']].rename(columns={'esp6500siv2_aal':'esp6500siv2_aa'})

        #freq_info = df_selected_variant[['esp6500siv2_all', '1000g2015aug_all', 'ExAC_ALL']]
        #freq_table = dbc.Table.from_dataframe(freq_info, striped=True, bordered=True, hover=True)



        names = [freq_eur, freq_afr, freq_amr, freq_as, freq_1000g, freq_exac, freq_gnomad, freq_esp]
        titles = ['European', 'African', 'Amerindian', 'Asian', '1000 Genomes', 'ExAC', 'gnomAD', 'ESP 6500']
        freq_table = [0] * len(names)
        for i in range(len(names)):
            color_bar = [0] * len(names[i].values[0])
            for j in range(len(names[i].values[0])):
                if names[i].values[0][j] < 0.001:
                    color_bar[j] = px.colors.qualitative.Set1[0]
                elif 0.001 <= names[i].values[0][j] < 0.01:
                    color_bar[j] = '#fcc323'
                elif 0.01 <= names[i].values[0][j] < 0.05:
                    color_bar[j] = px.colors.qualitative.Vivid[2]
                elif names[i].values[0][j] >= 0.05:
                    color_bar[j] = px.colors.qualitative.Vivid[5]
                else:
                    color_bar[j] = 'black'
            data = [go.Bar(x = names[i].values[0], y = names[i].columns.values, orientation='h', width=0.3, marker={'color':color_bar})]
            layout = go.Layout(title = titles[i], title_x = 0.5, title_y = 0.85, xaxis=dict(range=[0, freq_m]), xaxis_title="Frecuencia")
            freq_table1 = go.Figure(data = data, layout = layout)
            #freq_table[i] = dcc.Graph(figure= {'data':[{'x':names[i].values[0], 'y':names[i].columns.values}], 'type':'bar'})
            freq_table[i] = dcc.Graph(figure = freq_table1,  style= {'height':400}, config = {'displayModeBar': False})


        #freq_info2 = df_selected_variant.iloc[:, 32:40]
        #freq_table2 = dbc.Table.from_dataframe(freq_info2, striped=True, bordered=True, hover=True)

        #####################################################################################################
        # Predictors
        #####################################################################################################
        # TODO: add information about each score (and links)
        # TODO: create function to transform predictions

        # GRANTHAM SCORE
        # ----------------------------------------------------------------------------------------------------
        grantham_pred = np.NaN
        grant =  df_selected_variant['grantham_score'][0]
        if grant <= 50:
            grantham_pred = 'Conservativo'
        elif 51 <= grant < 101:
            grantham_pred = 'Moderadamente conservativo'
        elif 101 <= grant < 151:
            grantham_pred = 'Moderadamente radical'
        elif grant >= 151:
            grantham_pred = 'Radical'

        grantham = [html.A(html.Span(["Grantham Score ",html.I(className='fas fa-external-link-alt')]), href="https://pubmed.ncbi.nlm.nih.gov/4843792/", target="_blank"), grant,  grantham_pred]

        #SIFT
        #----------------------------------------------------------------------------------------------------
        sift_pred = np.NaN
        sift_score =  df_selected_variant['SIFT_pred'][0]
        if sift_score == "T":
                sift_pred  = "Tolerado"
        elif sift_score == "D":
                sift_pred = "Dañino"

        sift = [html.A(html.Span(["SIFT ",html.I(className='fas fa-external-link-alt')]), href="https://sift.bii.a-star.edu.sg/", target="_blank"), round(df_selected_variant['SIFT_score'][0],2), sift_pred]

        # LRT
        # ----------------------------------------------------------------------------------------------------
        lrt_pred = np.NaN
        lrt_score = df_selected_variant['LRT_pred'][0]
        if lrt_score == "D":
            lrt_pred = "Deletéreo"
        elif lrt_score == "N":
            lrt_pred = "Neutral"
        elif lrt_score == "U":
            lrt_pred = "Desconocido"

        lrt = [html.A(html.Span(["LRT ",html.I(className='fas fa-external-link-alt')]), href="https://genome.cshlp.org/content/19/9/1553.short", target="_blank"), round(df_selected_variant['LRT_score'][0],2), lrt_pred]


        # Mutation Taster
        # ----------------------------------------------------------------------------------------------------
        mut_pred = np.NaN
        mut_score = df_selected_variant['MutationTaster_pred'][0]
        if mut_score == "A":
            mut_pred = "Causante de enfermedad automático"
        elif mut_score == "D":
            mut_pred = "Causante de enfermedad"
        elif mut_score == "P":
            mut_pred = "Polimorfismo automático"
        elif mut_score == "N":
            mut_pred = "Polimorfismo"

        mut_taster = [html.A(html.Span(["Mutation Taster ",html.I(className='fas fa-external-link-alt')]), href="https://www.mutationtaster.org/", target="_blank"), round(df_selected_variant['MutationTaster_score'][0], 2), mut_pred]

        # PolyPhen-2
        # ----------------------------------------------------------------------------------------------------

        polyphen_2_hvar = [html.A(html.Span(['PolyPhen2 HVAR ',html.I(className='fas fa-external-link-alt')]), href="http://genetics.bwh.harvard.edu/pph2/", target="_blank"), round(df_selected_variant['Polyphen2_HVAR_score'][0], 2),
                           common.to_pred_polyphen(df_selected_variant['Polyphen2_HVAR_pred'][0])]
        polyphen_2_div = [html.A(html.Span(['PolyPhen2 HDIV ',html.I(className='fas fa-external-link-alt')]), href="http://genetics.bwh.harvard.edu/pph2/", target="_blank"), round(df_selected_variant['Polyphen2_HDIV_score'][0],2),
                           common.to_pred_polyphen(df_selected_variant['Polyphen2_HDIV_pred'][0])]

        # CADD
        # ----------------------------------------------------------------------------------------------------
        cadd_phred = round(df_selected_variant['CADD_phred'][0],2)
        if cadd_phred <= 20:
            cadd_pred = 'Tolerado (mayor score, mayor patogenicidad)'
        elif cadd_phred > 20:
            cadd_pred = 'Deletéreo (mayor score, mayor patogenicidad)'
        else:
            cadd_pred = 'Desconocido'
        cadd = [html.A(html.Span(['CADD (Phred) ',html.I(className='fas fa-external-link-alt')]), href="https://cadd.gs.washington.edu/", target="_blank"), cadd_phred, cadd_pred]


        # Construct the table with physicochemical impact predictors
        pred_names = ["Predictor", "Score", "Predicción"]
        predict =[grantham, sift,lrt, mut_taster,  polyphen_2_hvar, polyphen_2_div, cadd]
        df = pd.DataFrame(predict, columns=pred_names)

        for i in range(len(df["Predicción"])):
            df["Predicción"][i] = html.Span(df["Predicción"][i], style={"color": common.color_prediction(df["Predicción"][i])})

        #sc = df_selected_variant[['grantham_score', 'SIFT_score', 'SIFT_pred', 'CADD_raw_rankscore', 'LRT_score']]
        sc_table1= dbc.Table.from_dataframe(df, style={'text-align':'center'})

        # PhyloP
        # ----------------------------------------------------------------------------------------------------

        phylop100 = [html.A(html.Span(['phyloP 100 Way Vertebrate ',html.I(className='fas fa-external-link-alt')]), href="http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phyloP17way/", target="_blank"), round(df_selected_variant['phyloP100way_vertebrate'][0],2),
                           common.get_prediction(df_selected_variant['phyloP100way_vertebrate'][0],'phyloP')]

        phylop20 = [html.A(html.Span(['phyloP 20 Way Mammalian ',html.I(className='fas fa-external-link-alt')]), href="http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phyloP30way/", target="_blank"), round(df_selected_variant['phyloP20way_mammalian'][0],2),
                     common.get_prediction(df_selected_variant['phyloP20way_mammalian'][0],'phyloP')]

        # Siphy
        # ----------------------------------------------------------------------------------------------------
        siphy = [html.A(html.Span(['SiPhy (29-way) log Odds ',html.I(className='fas fa-external-link-alt')]), href="https://www.broadinstitute.org/mammals-models/29-mammals-project-supplementary-info", target="_blank"), round(df_selected_variant['SiPhy_29way_logOdds'][0],2),
                           common.get_prediction(df_selected_variant['SiPhy_29way_logOdds'][0],'SiPhy')]

        # GERP
        # ----------------------------------------------------------------------------------------------------
        gerp = [html.A(html.Span(['GERP ',html.I(className='fas fa-external-link-alt')]), href="http://mendel.stanford.edu/SidowLab/downloads/gerp/index.html", target="_blank"), round(df_selected_variant['GERP++_RS'][0], 2),
                 common.get_prediction(df_selected_variant['GERP++_RS'][0], 'GERP')]


        # Construct the table with evolutive conservation predictors
        pred_names = ["Predictor", "Score", "Predicción"]
        predict = [phylop100, phylop20, siphy, gerp]
        df = pd.DataFrame(predict, columns=pred_names)
        for i in range(len(df["Predicción"])):
            df["Predicción"][i] = html.Span(df["Predicción"][i],
                                            style={"color": common.color_prediction(df["Predicción"][i])})

        sc_table2 = dbc.Table.from_dataframe(df, style={'text-align': 'center'})

        # Construct the table with evolutive conservation predictors
        sc_table3 = []




        #####################################################################################################
        # Rules
        #####################################################################################################

        button_acmg = html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Interpretación"]),
                             href="https://www.nature.com/articles/gim201530.pdf", target="_blank", style={"font-size": "14px"})

        variant_rules = df_selected_variant.iloc[:,156:183].T.reset_index()
        variant_rules.columns = ['rule', 'value']
        result = pd.merge(variant_rules, df_rules, on=["rule", "rule"])
        result["color"] = np.nan
        result.at[:4, ["color"]] = "#c73537"
        result.at[5:10, ["color"]] = "#d77b56"
        result.at[11:15, ["color"]] = "#708846"
        result.at[16:20, ["color"]] = "#623a7d"
        result.at[21:26, ["color"]] = "#5080ad"
        filtered = result.query('value == 1')['rule'].to_list()
        rules_mate = html.A(html.Span([html.I("Reglas que se cumplen: "+ ", ".join(filtered))], style={"font-size": "18px", "color":"black"}))

        table_header = [
            html.Thead(html.Tr([html.Th("Regla"), html.Th("Explicación")]))
        ]
        a = []
        for index, row in result.iterrows():
            row = html.Tr([html.Td(row['rule']), html.Td(row['detailed_rule'])],
                          style={'background-color': row['color'], 'color':'white'}) if (row['value'] == 1) else (
                    html.Tr([html.Td(row['rule'], style={'color':row['color']}), html.Td(row['detailed_rule'])]))  if (row['value'] == 0) else ( html.Tr([html.Td(row['rule']), html.Td(row['detailed_rule'])], style={'background-color': "#cccccc", 'color':'white'}))
            a.append(row)
        table_body = [html.Tbody(a)]
        table_rules = dbc.Table(table_header + table_body, bordered=False)
        #sc_table2 = dbc.Table.from_dataframe(df_rules.rename(columns = {'rule':'Regla', 'detailed_rule':'Explicación'}), style={'text-align': 'center'})

        coll_2 = html.Div([
            dbc.Row([html.A(html.Span([html.I(className='fas fa-newspaper'),
                              " Standards and guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College of Medical Genetics and Genomics and the Association for Molecular Pathology"]),
                   href="https://www.nature.com/articles/gim201530.pdf", target="_blank")
                     ]),
            dbc.Row([html.A(html.Span([html.I(className='fas fa-newspaper'),
                                       " Sherloc: a comprehensive refinement of the ACMG-AMP variant classification criteria"]),
                            href="https://www.gimjournal.org/article/S1098-3600(21)01370-8/pdf", target="_blank")
                     ])
        ])

        #####################################################################################################
        # Sample
        #####################################################################################################

        if pd.isna(df_selected_variant['patient'][0]):
            df1 = pd.DataFrame(columns=['Fenotipo', 'Herencia', 'Cigosidad', 'Sexo'])
            df1 = df1.append({'Fenotipo': '-', 'Herencia': '-', 'Cigosidad': '-', 'Sexo': '-'}, ignore_index=True)
            sample_table_1 = dbc.Table.from_dataframe(df1)
            df2 = pd.DataFrame(columns=['Familia afectada', 'Etnicidad', 'Origen de la muestra'])
            df2 = df2.append({'Familia afectada': '-', 'Etnicidad': '-', 'Origen de la muestra': '-'}, ignore_index=True)
            sample_table_2 = dbc.Table.from_dataframe(df2)


        else:
            print('hola', df_selected_variant['patient'][0])
            api_route_sample = '/sample/' + df_selected_variant['patient'][0]
            status_sample, df_sample = common.api_request(api_route_sample, 'GET')

            sample_info = df_sample[['phenotype', 'inheritance', 'zigocity', 'sex']]
            sample_info = sample_info.rename(
                columns={'phenotype': 'Fenotipo', 'inheritance': 'Herencia', 'zigocity': 'Cigosidad', 'sex': 'Sexo'})
            sample_table_1 = dbc.Table.from_dataframe(sample_info)

            sample_info = df_sample[['family_affected', 'ethnicity', 'origin']]
            sample_info = sample_info.rename(
                columns={'family_affected': 'Familia afectada', 'ethnicity': 'Etnicidad',
                         'origin': 'Origen de la muestra'})
            sample_table_2 = dbc.Table.from_dataframe(sample_info)



        #####################################################################################################
        # Submit
        #####################################################################################################

        options_label=[{'label': i, 'value': i} for i in df_labels.label]
        options_comments=[{'label': i, 'value': i} for i in df_comments.comment]
        options_default_value = ""  # allows to reset the dropdowns to the default value


        table_download = dash_table.DataTable(
            id='data_download',
            columns=[
                {"name": i, "id": i, "deletable": False, "selectable": True} \
                for i in  df_selected_variant.columns
            ],
            data=df_selected_variant.to_dict('records'),
            # editable=True,
            export_format='xlsx',
            export_columns= 'all',
            merge_duplicate_headers=True,
            hidden_columns= list(set(df_selected_variant.columns)-set(['Chr', 'Gene.refGene', 'Start', 'End', 'Ref', 'Alt', 'Func.refGene'])),
            style_cell={
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
                'maxWidth': '500px',
                'textAlign': 'center',
                'color': colors['text_7'],

            },
            style_data={
                'font-family': "Open sans light, sans-serif",
                'font_size': '14px',
            },
            style_header={
                'backgroundColor': 'rgb(235, 235, 235)',
                'fontWeight': 'bold',
                'font-family': "Open-sans, sans-serif"
            },
            style_as_list_view=True,
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }
            ],
            # style_as_list_view=True,
        )

        return [title, tag, tt2, is_acmg, tt, qual, dp, basic_table, basic_table2, basic_table3, coll, button_clinvar,
                name_clinvar, clinvar_table,  clinvar_table5, clinvar_table6, button_omim, omim_table, omim_table_2,
                freq_max, freq_table[0], freq_table[1], freq_table[2], freq_table[3], freq_table[4], freq_table[5],
                freq_table[6], freq_table[7], sc_table1, sc_table2, sc_table2, rules_mate, button_acmg, table_rules, coll_2,
                sample_table_1, sample_table_2, options_label, options_default_value, options_comments,
                options_default_value, "tab_1", "tab_freq_1", table_download,  {'display': 'block'}]
    else:
        raise dash.exceptions.PreventUpdate  # avoid update of elements


@app.callback(
    Output("download", "data"),
    [Input('btn', 'n_clicks')]
   )
def download_variant(n_clicks):
    context_id = common.get_trigger_id()
    if context_id == 'btn':
        method = 'GET'
        global selected_variant_id_conf
        api_route = '/variants/' + str(selected_variant_id_conf)
        status_conf_down, df_variants_conf_down = common.api_request(api_route, method)
        return send_data_frame(df_variants_conf_down.to_excel, "variant_"+df_variants_conf_down['ID_int'].to_string(index=False).strip()+".xls", index=False)


@app.callback(
    [
        Output('msg-classification-feedback2', 'children'),
        Output('msg-classification-feedback2', 'color'),
        Output('msg-classification-feedback2', 'style')
    ],
    [
        Input('btn-submit-classification2', 'n_clicks'),
        Input('combo-labels2', 'value'),
        Input('combo-comments', 'value')
    ]
)
def show_classification_feedback(n_clicks, selected_label, selected_comment):
    """
    Show feedback based on user classification
    :param n_clicks:
    :param selected_label: selected value in classification combobox
    :param selected_comment: selected value in comments combobox
    :return: [string, string, style]
    """
    context_id = common.get_trigger_id()


    if context_id == 'btn-submit-classification2':
        global selected_variant_id_conf

        # send classification data to API
        comment_ID = df_comments.loc[df_comments['comment'] == selected_comment]['ID']
        print(comment_ID) 

        route = '/users/classifications/set'
        method = 'POST'
        data = {'user_id': current_user.user, 'variant_id': selected_variant_id_conf, 'label_id': selected_label,
                'comment_id': comment_ID}
        status_conf, response = common.api_request(route, method, data, False)
        route_2 = '/consensus/set'
        data_2 = {'variant_id': selected_variant_id_conf}
        status_cons, response_cons = common.api_request(route_2, method, data_2, False)

        route_update_c = '/users/points/classification'
        data_update_c = {'user_id': current_user.user}
        status_update_c, response_update_c = common.api_request(route_update_c, method, data_update_c, False)

        istrain, responsetrain = common.api_request('/training/start', method)
        print(istrain)
        print('comment', selected_comment)
        
        if status_conf == 'ok':
            msg = "¡Muchas gracias por tu aporte!"
            color = "success"
        else:
            if response == 'IntegrityError':
                # variant already classified
                msg = html.Span("You've already classified variant " + str(selected_variant_id_conf))
                color = "warning"
            elif response == 'APIError':
                # API connection error
                msg = [html.Strong("API connection error:"), html.Span(" the classification could not be saved.")]
                color = "danger"
            else:
                msg = [html.Strong("Internal error:"), html.Span(" the classification could not be saved.")]
                color = "danger"

        style = {'display': 'block'}
    else:
        msg = ''
        color = ''
        style = {'display': 'none'}

    return [msg, color, style]





