import config
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
import numpy as np
from pandas import DataFrame
from dash_extensions import Download
from dash_extensions.snippets import send_data_frame

# import requests
import common
from dash.dependencies import Output, Input ,State
from server import app
from dash import no_update
from flask_login import logout_user, current_user
# import json

login_alert = dbc.Alert(
    'User not logged in. Taking you to login.',
    color='danger'
)

colors = {
    'background': '#FFFFFF',
    'background_2': '#e5f5f2',
    'background_3': '#D0E3F5',
    'text_1': '#74807E',
    'text_2': '#008080',
    'text_3': '#FF5733',
    'text_4': '#8E7F7A',
    'text_5': '#193176',
    'text_6': '#45B39D',
    'text_7': '#202a3d',
    'pills_1': '#E5E5E5',
    'conflicting':'#F77910'}

tab_selected_style = {
    'borderTop': '3px solid #E36209 !important'
}



location = dcc.Location(id='page1-url', refresh=True)
expertise_level = ['Novice', 'Advanced Beginner', 'Competent', 'Proficient', 'Expert']
expected_label = ''  # expected label (classification) of variant, to compare with user answer
selected_variant_id = ''  # selected variant id from the main table

# API requests to be made when the page loads
api_route_variants = '/variants/nonconf'
status, df_variants = common.api_request(api_route_variants)  # load non-conflictive variants


method = 'POST'
data = {'is_classif': '0'}
api_route_labels = '/labels'
status_labels, df_labels = common.api_request(api_route_labels, method, data)  # load labels

api_route_rules = '/rules'
status_rules, df_rules = common.api_request(api_route_rules)  # load rules

def layout():
    api_route_variants = '/variants/nonconf'
    data = {'user': current_user.user}
    status, df_variants = common.api_request(api_route_variants, 'POST', data)
    df_variants = df_variants.rename(
        columns={'AAChange.refGene': 'Cambio AA', 'Func.refGene': 'Región', 'ExonicFunc.refGene': 'Función exónica',
                 'Gene.refGene': 'Gen', 'avsnp150': 'rsID', 'freq.max': 'Frec. max'}).round(4)

    if (current_user.level == 'Experto'):
        message = html.H6(children=[html.I(className='fas fa-user-tag'), "  Usted cuenta con un nivel de "+current_user.level+", se sugiere pasar a la etapa de etiquetado. ", html.A(children=html.I(className='fas fa-arrow-right'), href="/classification" )],  style={"font-weight": "bold"})
    elif (current_user.level == 'Avanzado') :
        message = html.H6(children=[html.I(className='fas fa-user-tag'),"  Usted cuenta con un nivel de "+current_user.level+", se sugiere completar una clasificación y pasar a la etapa de etiquetado. ", html.A(children=html.I(className='fas fa-arrow-right'), href="/classification" )], style={"font-weight": "bold"})

    else:
        message = html.H6(children=[html.I(className='fas fa-user-tag'),"  Usted cuenta con un nivel de "+current_user.level+", ¡a seguir trabajando!"])


    return dbc.Row(
        dbc.Col(
            [
                location,
                dbc.Card(
                    dbc.CardBody([
                        html.H5("Etapa 1", className="lead", style={'color': colors['text_7']}
                                ),
                        html.H2("Clasificación de variantes y entrenamiento de usuarios", style={'color': colors['text_7']}),
                        html.Hr(className="my-2"),
                        html.Div([
                            dbc.Button(
                                children=html.Span([html.I(className='fas fa-info-circle'), ' Más información']),
                                id='open', color="primary"),
                            dbc.Modal([
                                dbc.ModalHeader("Clasificación de variantes y entrenamiento de usuarios"),
                                dbc.ModalBody("Esta página está dedicada a la clasificación de variantes con el fin de entrenar a usuarios."
                                              "Al usuario se le presentarán variantes de distintos tipos, que ya cuentan con una etiqueta conocida, con un alto grado de consenso."),
                                dbc.ModalFooter(
                                    dbc.Button("Close", id="close", className="ml-auto")
                                ),
                            ],
                                id="modal",
                            )
                        ]),
                    ]), color="light"
                ),

                html.Br(),
                html.Br(),
                dbc.Card(
                    dbc.CardBody(
                        html.Div(
                            dbc.Row(
                                dbc.Col(message)))
                    )),
                html.Br(),
                html.Br(),

                html.P(children='Por favor, seleccione una variante de la lista:',
                       style={'fontWeight': 'normal', 'color': colors['text_7']}),

                dash_table.DataTable(
                    id='datatable-interactivity',
                    columns=[
                        {"name": i, "id": i, "deletable": False, "selectable": True} \
                        for i in df_variants.loc[:, df_variants.columns != 'ID'].columns
                    ],
                    data=df_variants.to_dict('records'),
                    # editable=True,
                    filter_action="native",
                    sort_action="native",
                    sort_mode="multi",
                    # column_selectable="single",
                    row_selectable="single",
                    row_deletable=False,
                    selected_columns=[],
                    selected_rows=[],
                    page_action="native",
                    page_current=0,
                    page_size=7,
                    # style_table={'overflowX': 'scroll'},
                    style_cell={
                        'overflow': 'hidden',
                        'textOverflow': 'ellipsis',
                        'maxWidth': '250px',
                        'textAlign': 'center',
                        'color': colors['text_7'],

                    },
                    style_data={
                        'font-family': "Open sans light, sans-serif",
                        'font_size': '14px',
                    },
                    # tooltip_data=[
                    #     {
                    #         column: {'value': str(value), 'type': 'markdown'}
                    #         for column, value in row.items()
                    #     } for row in df_variants_conf.to_dict('rows')
                    # ],
                    # tooltip_duration=None,
                    style_header={
                        'backgroundColor': 'rgb(235, 235, 235)',
                        'fontWeight': 'bold',
                        'font-family': "Open-sans, sans-serif"
                    },
                    # tooltip_data=[
                    #   {
                    #      column: {'value': str(value), 'type': 'markdown'}
                    #     for column, value in row.items()
                    # } for row in df_variants_conf.to_dict('records')
                    # ],
                    # css=[{
                    #    'selector': '.dash-table-tooltip',
                    #    'rule': 'placement: auto'
                    # }],
                    # tooltip_duration=None,
                    # tooltip_delay=0,
                    style_as_list_view=True,
                    style_data_conditional=[
                        {
                            'if': {'row_index': 'odd'},
                            'backgroundColor': 'rgb(248, 248, 248)'
                        }
                    ],
                    # style_as_list_view=True,
                ),
                html.Br(),
                html.Div(id='datatable-interactivity-container'),
                html.Br(),
                dbc.Button(children=html.Span([html.I(className='fas fa-eye'), ' Ver detalles']), id='btn-view-details',
                           n_clicks=0, color='primary'),
                html.Br(),
                html.Br(),
                html.P(children='O tome una variante conflictiva de forma aleatoria',
                       style={'color': colors['text_7']}),
                html.Br(),
                dbc.Button(children=html.Span([html.I(className='fas fa-random'), ' Selección aleatoria']),
                           id='btn-random-variant',
                           n_clicks=0, color='primary'),
                html.Br(),
                html.Br(),
                html.Br(),
                html.Div(
                    id='details-container',
                    children=[
                        dbc.Card(
                            [
                                # html.H3(id='variant-info-title-conf', children='Variant info', style={'fontWeight': 'normal','color': colors['text_2']}),
                                html.Div(id='variant-info-title',
                                         style={"maxHeight": "130px", "overflow": "scroll", 'font-size': '26px',
                                                'fontWeight': 'normal', 'color': colors['text_7'
                                                                                        '']})
                            ],
                            body=True,
                            style={"backgroundColor": colors['background_2']}
                        ),
                        html.Br(),
                        # basic info
                        dbc.Tabs(id="tabs_train", active_tab="tab_1", children=[
                            dbc.Tab(tab_id="tab_1",label='General', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Información general',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'], 'overflow': 'hidden',
                                               'textOverflow': 'ellipsis'}),
                                html.Br(),
                                html.Div([
                                    dbc.Row([
                                        dbc.Col([
                                            html.Div(id='acmg_train',
                                                     style={'margin-bottom': '20px', 'text-align': 'center'}),
                                            html.Div(id='tt')
                                        ], width=2),
                                        dbc.Col([
                                            html.H6('Quality'),
                                            html.Div(id='quality_train')
                                        ], width=2),
                                        dbc.Col([
                                            html.H6('DP (Read depth)'),
                                            html.Div(id='dp_train')
                                        ], width=2)
                                    ])
                                ], style={'text-align': 'center'}),
                                html.Br(),
                                dbc.Card(
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-basic', style={'text-align': 'center'}),
                                                html.Div(id='variant-info-basic2', style={'text-align': 'center'}),
                                                html.Div(id='variant-info-basic3', style={'text-align': 'center'})
                                            ])
                                        ])
                                    ])
                                ),
                            html.Br(),
                            html.Br(),
                            html.Br()
                            ], tab_style ={'color': colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="tab_2", label='Asociaciones', children=[
                            # asociations
                                html.Br(),
                                html.Br(),
                                html.H3('Asociaciones',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'],
                                               'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                # OMIM info
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("OMIM")
                                            ]),
                                            dbc.Col([
                                                html.Div(id='button_omim_train', style={'textAlign': 'right'})
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '23px'}),
                                    dbc.CardBody([
                                        html.Div(id='variant-info-omim'),
                                        # html.H4('OMIM', style={'fontWeight': 'normal','color': colors['text_4'],'overflow': 'hidden','textOverflow': 'ellipsis'}),
                                        html.Br(),
                                        html.H5("Relación Gen-Fenotipo", className="card-title"),
                                        html.Div(id='variant-info-omim_2'),
                                        html.Br()
                                    ])
                                ]),
                            ], tab_style={"color": colors['text_1'], 'tab-size': '5'}),
                            dbc.Tab(tab_id="tab_3", label='Clasificación online', children=[
                                # asociations
                                html.Br(),
                                html.Br(),
                                html.H3('Clasificación online',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'],
                                               'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                # clinvar info
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("ClinVar")
                                            ]),
                                            dbc.Col([
                                                html.Div(id='button_clinvar_train', style={'textAlign': 'right'})
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '23px'}),
                                    dbc.CardBody([
                                        # html.H4('CLINVAR',style={'fontWeight': 'normal','color': colors['text_4'],'overflow': 'hidden','textOverflow': 'ellipsis'}),

                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-clinvar0'),
                                                html.Br(),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-clinvar',
                                                                 style={'text-align': 'center'}),
                                                    ])
                                                ]),
                                                html.Br(),
                                                html.Br(),
                                                html.H5("Interpretaciones realizadas en ClinVar",
                                                        className="card-title"),
                                                html.Div(id='variant-info-clinvar2',
                                                         style={'height': '300px', 'overflowY': 'ellipsis',
                                                                'overflow': 'scroll', 'textOverflow': 'ellipsis'}),
                                                html.Br(),
                                                html.H5("Conflictos en interpretaciones", className="card-title"),
                                                html.Div(id='variant-info-clinvar3',
                                                         style={'height': '200px', 'overflowY': 'ellipsis',
                                                                'overflow': 'scroll', 'textOverflow': 'ellipsis'})
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # OMIM info

                            ], tab_style={"color": colors['text_1'], 'margin': '0px'}),
                            # frequencies
                            dbc.Tab(tab_id="tab_4", label='Frequencias', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Frequencias poblacionales',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'],
                                               'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                dbc.Card(
                                    dbc.CardBody([
                                        html.Br(),
                                        html.Div(id='variant-info-freq1'),
                                        html.Br(),
                                        dbc.Tabs(id="tabs_freq_train", active_tab="tab_freq_1", children=[
                                            dbc.Tab(tab_id="tab_freq_1", label='Población', children=[
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop1')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop2')
                                                    ])
                                                ]),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq2')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq3')
                                                    ])
                                                ])
                                            ]),
                                            dbc.Tab(tab_id="tab_freq_2", label='Proyecto', children=[
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop3')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-pop4')
                                                    ])
                                                ]),
                                                dbc.Row([
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq4')
                                                    ]),
                                                    dbc.Col([
                                                        html.Div(id='variant-info-freq5')
                                                    ])
                                                ])
                                            ])
                                        ]),
                                        html.Div(children=html.Span([html.I(className='fas fa-circle', style={
                                            'color': px.colors.qualitative.Set1[0]}), ' f<0.1%  ',
                                                                     html.I(className='fas fa-circle',
                                                                            style={'color': '#fcc323'}),
                                                                     '  0.1%<f<1%  ',
                                                                     html.I(className='fas fa-circle', style={
                                                                         'color': px.colors.qualitative.Vivid[2]}),
                                                                     '  1%<f<5%  ',
                                                                     html.I(className='fas fa-circle', style={
                                                                         'color': px.colors.qualitative.Vivid[5]}),
                                                                     ' f>5%  ']))
                                    ])
                                ),
                                html.Br()], tab_style ={"color": colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="5", label='Predictores', children=[
                                html.Br(),
                                html.Br(),
                                html.H3('Predictores', style={'fontWeight': 'normal', 'color': colors['text_7'],
                                                              'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                # Predictores impacto fisicoquímico
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Impacto Fisicoquímico")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-fq-scores1')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # Predictores conservación evolutiva
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Conservación Evolutiva")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-evo-scores1')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                # Predictores splicing
                                dbc.Card([
                                    dbc.CardHeader(children=[
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div("Splicing")
                                            ])
                                        ])
                                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-splicing-scores')
                                            ])
                                        ])
                                    ])
                                ]),
                                html.Br(),
                                html.Br(),
                                html.Br()
                            ], tab_style={"color": colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="tab_8", label='Muestra', children=[
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.H3('Información de la muestra',
                                        style={'fontWeight': 'normal', 'color': colors['text_7'],
                                               'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                dbc.Card(
                                    dbc.CardBody([
                                        dbc.Row([
                                            dbc.Col([
                                                html.Div(id='variant-info-sample1',
                                                         style={'text-align': 'center'}),
                                                html.Div(id='variant-info-sample2',
                                                         style={'text-align': 'center'}),
                                            ])
                                        ])
                                    ])
                                ),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                            ], tab_style={"color": colors['text_1'], 'margin': '0px'}),
                            dbc.Tab(tab_id="tab_7", label='Etiquetado', children=[
                                html.Br(),
                                html.Br(),
                                # user classification
                                html.H3('Su clasificación', style={'fontWeight': 'normal', 'color': colors['text_7'],
                                                              'overflow': 'hidden', 'textOverflow': 'ellipsis'}),
                                html.Br(),
                                html.H6(
                                    children= html.Span([html.I(className='fas fa-tag'), ' De acuerdo con la información proporcionada, establezca una clasificación para la variante que está viendo actualmente. Corresponde a:'])),
                                dcc.Dropdown(
                                    id='combo-labels',
                                    options=[{'label': i, 'value': i} for i in df_labels.label],
                                    value='',
                                    placeholder="Seleccionar la patogenicidad para la variante..."
                                ),
                                html.Br(),
                                dbc.Button(
                                    children=html.Span([html.I(className='fas fa-paper-plane'), ' Emitir una clasificación']),
                                    id='btn-submit-classification', n_clicks=0, color='primary'),
                                html.Br(),
                                html.Br(),
                                dbc.Alert(id='msg-classification-feedback', children='', color='danger',
                                          style={'display': 'none'}),
                                html.Br(),
                                html.Br(),
                                html.Br(),

                                dbc.Col([
                                    html.Div(id='rules_mate', children='')
                                ]),
                                html.Br(),
                                dbc.Col([
                                    html.Div(id='table_rules', children=''),
                                    # html.Div(id='variant-info-basic-conf2', style={'text-align': 'center'}),
                                    # html.Div(id='variant-info-basic-conf3', style={'text-align': 'center'})
                                ]),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                            ])
                        ], style ={'font-size': '17px'})
                    ],
                    style={'display': 'none'},
                ),

            ], width=12)
    )
    # if current_user.is_authenticated:


@app.callback(
    [
        Output('variant-info-title', 'children'),
        Output('acmg_train', 'children'),
        Output('tt_train', 'children'),
        Output('quality_train', 'children'),
        Output('dp_train', 'children'),
        Output('variant-info-basic', 'children'),
        Output('variant-info-basic2', 'children'),
        Output('variant-info-basic3', 'children'),
        Output('button_omim_train', 'children'),
        Output('variant-info-omim', 'children'),
        Output('variant-info-omim_2', 'children'),
        Output('button_clinvar_train', 'children'),
        Output('variant-info-clinvar0', 'children'),
        Output('variant-info-clinvar', 'children'),
        Output('variant-info-clinvar2', 'children'),
        Output('variant-info-clinvar3', 'children'),
        Output('tabs_freq_train', 'active_tab'),
        Output('variant-info-freq1', 'children'),
        Output('variant-info-pop1', 'children'),
        Output('variant-info-pop2', 'children'),
        Output('variant-info-freq2', 'children'),
        Output('variant-info-freq3', 'children'),
        Output('variant-info-pop3', 'children'),
        Output('variant-info-pop4', 'children'),
        Output('variant-info-freq4', 'children'),
        Output('variant-info-freq5', 'children'),
        Output('variant-info-fq-scores1', 'children'),
        Output('variant-info-evo-scores1', 'children'),
        Output('variant-info-splicing-scores', 'children'),
        Output('variant-info-sample1', 'children'),
        Output('variant-info-sample2', 'children'),
        Output('tabs_train', 'active_tab'),
        Output('details-container', 'style'),
        Output('combo-labels', 'value'),  # allows to reset the dropdown to the default value

        # Output('msg-classification-feedback', 'style'),
        # Output('btn-submit-classification', 'disabled')
    ],
    [
        Input('datatable-interactivity', 'selected_rows'),
        Input('btn-view-details', 'n_clicks'),
        Input('btn-random-variant', 'n_clicks'),
        # Input('btn-submit-classification', 'n_clicks'),
        # Input('combo-labels', 'value')
    ]
)
def view_variant(sel_rows, n_clicks_view, n_clicks_random):
    """
    Load and display information of the selected variant
    :param sel_rows: selected rows indices
    :param n_clicks_view: n clicks in the 'show info' button
    :param n_clicks_random: n clicks in the 'random' button
    :return: [title, table, table, table, style]
    """
    context_id = common.get_trigger_id()
    # print('element id:', context_id)

    if (len(sel_rows) == 1 and context_id == 'btn-view-details') or (context_id == 'btn-random-variant'):
        # avoid errors when no row is selected, by checking length of sel_rows

        global selected_variant_id
        route = '/variants'
        method = 'GET'

        if context_id == 'btn-random-variant':
            # request a random variant to API
            # data = {'variant_id': -1}  # set id to -1 to request a random variant
            route = route + '/nonconf/random'
            data = {'user': current_user.user, 'selection' : 1}
            status, df_selected_variant = common.api_request(route, 'POST', data, True)

            # get the ID of the random variant

            selected_variant_id = df_selected_variant.at[0, 'ID']
            # get the variant AA change
            selected_variant_AAchange = df_selected_variant.at[0, 'AAChange.refGene']
        else:
            # get variant id from initially loaded dataframe
            api_route_variants = '/variants/nonconf'
            data = {'user': current_user.user}
            status, df_variants = common.api_request(api_route_variants, 'POST', data)
            selected_variant_id = df_variants.at[sel_rows[0], 'ID']
            # get the variant AA change from initially loades dataframe
            selected_variant_AAchange = df_variants.at[sel_rows[0], 'AAChange.refGene']
            print('\nSelected variant:', selected_variant_AAchange)



            # request variant data to API
            # data = {'variant_id': int(selected_variant_id)}
            route = route + '/' + str(selected_variant_id)
            status, df_selected_variant = common.api_request(route, method, True)

        global expected_label
        expected_label = df_selected_variant.at[0, 'ClinicalSignificance']
        # print('> expected label is:', expected_label, type(expected_label))

        conflict_route = '/variants/conflict/' + str(selected_variant_id)
        status_conflict_info, df_conflict_info = common.api_request(conflict_route, method)

        clinvar_submission_route = '/variants/clin_submission/' + str(selected_variant_id)
        status_clinvar_submission, df_clinvar_submission = common.api_request(clinvar_submission_route, method)

        # prepare variant info to display
        #title = 'Info of variant: ' + str(selected_variant_AAchange)
        if pd.isnull(selected_variant_AAchange):
            print('is null')
            title = 'Variante a etiquetar: ' + str(selected_variant_AAchange)
        else:
            spl = selected_variant_AAchange.split(",")
            spl = DataFrame(spl, columns=['Variante a etiquetar: '])
            title = dbc.Table.from_dataframe(spl, borderless=True, style={'font-size': '26px', 'fontWeight': 'normal',
                                                                          'color': colors['text_7']})


        # Basic info

        acmg = ["BRCA1", "BRCA2", "TP53", "STK11", "MLH1", "MSH2", "MSH6", "PMS2", "APC", "MUTYH", "VHL", "MEN1", "RET",
                "PTEN", "RB1", "SDHD", "SDHAF2", "SDHC", "SDHB", "TSC1", "TSC2", "WT1", "NF2", "COL3A1", "FBN1",
                "TGFBR1", "TGFBR2", "ACTA2", "MYH11", "MYBPC3", "MYH7", "TNNT2", "TNNI3", "TPM1", "MYL3", "ACTC1",
                "PRKAG2", "GLA", "MYL2", "LMNA", "RYR2", "PKP2", "DSP", "DSC2", "TMEM43", "DSG2", "KCNQ1", "KCNH2",
                "SCN5A", "LDLR", "APOB", "PCSK9", "RYR1", "CACNA1S", "PALB2", "MAX", "TMEM127", "BMPR1A", "SMAD4",
                "SMAD3", "CASQ2", "TRDN", "FLNC", "TTN", "BDT", "OTC", "GAA", "HFE", "ACVRL1", "ENG", "HNF1A", "RPE65",
                "ATP7B"]
        if df_selected_variant['Gene.refGene'].values in acmg:
            is_acmg = html.H4(dbc.Badge("ACMG", color="info", className="lml-1", pill=True, id="nacmg"))
        else:
            is_acmg = html.H4(dbc.Badge("ACMG", className="lml-1", pill=True, id="nacmg",
                                        style={'background-color': colors['pills_1']}))

        tt = dbc.Tooltip(
            "Si esta etiqueta está coloreada, indica que la variante está en un gen de relevancia preventiva.",
            target="nacmg")



        qual = dbc.Progress(value=df_selected_variant['QUAL'])
        dp = dbc.Progress(value=df_selected_variant['DP'])

        ###############################################################################################################################
        # Basic info
        ###############################################################################################################################

        basic_info = df_selected_variant[['Chr', 'cytoBand', 'Start', 'End', 'Ref', 'Alt']]
        basic_info = basic_info.rename(columns={'Chr': 'Cromosoma', 'cytoBand': 'Citobanda', 'Ref': 'REF', 'Alt': 'ALT'})
        basic_table = dbc.Table.from_dataframe(basic_info)

        basic_info2 = df_selected_variant[['Assembly', 'Gene.refGene', 'Func.refGene', 'ExonicFunc.refGene']]
        basic_info2 = basic_info2.rename(
            columns={'Gene.refGene': 'Gen', 'Func.refGene': 'Región', 'ExonicFunc.refGene': 'Función exónica'})
        basic_table2 = dbc.Table.from_dataframe(basic_info2)

        basic_info3 = df_selected_variant[['type_2', 'snp138', 'MC']].replace("_", " ", regex=True)
        basic_info3 = basic_info3.rename(
            columns={'type_2': 'Tipo de variante', 'snp138': 'RS ID (dbSNP 138)', 'MC': 'Consecuencia Molecular'})
        basic_table3 = dbc.Table.from_dataframe(basic_info3)

        ###############################################################################################################################
        # Associations info
        ###############################################################################################################################

        mim_number = str(df_selected_variant['MIM'].values).strip('[]')
        button_omim = html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Ver entrada del Gen en OMIM"]),
                             href=config.omim_base_url + mim_number, target="_blank", style={"font-size": "14px"})

        omim_info = str(df_selected_variant['MIM'].values).strip('[]')
        omim_table = html.H5(children=["Número MIM de Gen/Locus: ",
                                       html.A(omim_info, href=config.omim_base_url + mim_number, target="_blank")],
                             style={'color': colors['text_7']})

        if df_selected_variant['Phenotypes'].empty == False:
            omim_info_2 = df_selected_variant['Phenotypes'].str.split(";", expand=True).stack().to_frame().rename(columns={0:'Phenotype, Phenotype MIM number, Phenotype mapping key, Inheritance'})

            omim = pd.DataFrame()

            omim[['Fenotipo', 'Other']] = omim_info_2['Phenotype, Phenotype MIM number, Phenotype mapping key, Inheritance'].str.split(", (?=\d{3,}|}|])", expand=True)
            omim[['Número MIM Fenotipo','Other2']]= omim['Other'].str.split(" (?=\(\d\))",expand=True)
            omim[['Mapping key del fenotipo', 'Modo de herencia']] = omim['Other2'].str.split("\),", expand=True)
            omim['Mapping key del fenotipo'] = omim['Mapping key del fenotipo'].str.replace('[\(\)]', '')
            omim_final= omim[['Fenotipo','Número MIM Fenotipo','Mapping key del fenotipo','Modo de herencia']]

            # TODO: the tooltip has to correspond the mapping key conditionally
            for i in range(len(omim_final['Mapping key del fenotipo'])):
                omim_final['Mapping key del fenotipo'][0,i] = html.Div(
                    [html.A(html.Span([
                        omim_final['Mapping key del fenotipo'][0,i]], id="tooltip-key",style={"textDecoration": "underline", "cursor": "pointer"})),
                        dbc.Tooltip([html.P("(1): El trastorno se posicionó mediante el mapeo del gen de tipo salvaje."),
                                     html.P("(2): El fenotipo de la enfermedad en sí fue mapeado."),
                                     html.P("(3): Se conoce la base molecular del trastorno."),
                                     html.P("(4): El trastorno es un síndrome de deleción o duplicación cromosómica")
                                     ],
                                    target="tooltip-key"
                                    )
                    ])

            # TODO: the tooltip has appear only when the case appears
            for i in range(len(omim_final['Fenotipo'])):
                omim_final['Fenotipo'][0,i] = html.Div(
                    [html.A(html.Span([
                        omim_final['Fenotipo'][0,i]], id="tooltip-target",style={"textDecoration": "underline", "cursor": "pointer"})),
                        dbc.Tooltip([html.P("Los corchetes, '[ ]', indican 'no enfermedad', principalmente variaciones genéticas que dan lugar a valores de pruebas de laboratorio aparentemente anormales."),
                                     html.P("Las llaves, '{ }', indican mutaciones que contribuyen a la susceptibilidad a trastornos multifactoriales o a la susceptibilidad a infecciones."),
                                     html.P("Un signo de interrogación, '?', delante del nombre del fenotipo indica que la relación entre el fenotipo y el gen es provisional.")],
                                    target="tooltip-target"
                                    )
                    ])

            for i in range(len(omim_final['Número MIM Fenotipo'])):
                omim_final['Número MIM Fenotipo'][0,i] = [html.A(html.Span([omim_final['Número MIM Fenotipo'][0,i]," ",html.I(className='fas fa-external-link-alt')]), href=config.omim_base_url+omim_final['Número MIM Fenotipo'][0,i], target="_blank")]
            omim_table_2 = dbc.Table.from_dataframe(omim_final, striped=True, bordered=True, hover=True)
        else:
            omim_table_2 = []


    ###############################################################################################################################
    # ClinVar
    ###############################################################################################################################
        var_id = str(df_selected_variant['VariationID'].values).strip('[]')
        button_clinvar = html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Ver entrada en ClinVar"]),
                                href=config.clinvar_base_url + var_id, target="_blank", style={"font-size": "14px"})

        variant_name = str(df_selected_variant['Name'].values).strip("[']")
        name_clinvar = html.H5(
            children=["Variant name: ", html.A(variant_name, style={'color': colors['text_6'], 'font-weight': 'bold'})],
            style={'color': colors['text_6']})

        clinvar_info1 = df_selected_variant[['ClinicalSignificance', 'CLNSIGCONF']].replace("_", " ", regex=True)
        clinvar_info1 = clinvar_info1.rename(columns={'CLNSIG': 'Significado Clínico'})
        # clinvar_table1 = dbc.Table.from_dataframe(clinvar_info1, borderless =True, style = {'color':'orange'})

        clinvar_info = df_selected_variant[['CLNREVSTAT', 'LastEvaluated', 'NumberSubmitters']].replace("_", " ",
                                                                                                        regex=True)
        # clinvar_info = clinvar_info.replace("_", " ", regex=True)
        clinvar_info = clinvar_info.rename(
            columns={'CLNREVSTAT': 'Status de revisión', 'LastEvaluated': 'Última evaluación',
                     'NumberSubmitters': 'Número de remitentes'})
        # clinvar_table = dbc.Table.from_dataframe(clinvar_info, borderless =True)

        if clinvar_info[clinvar_info.columns.values[0]].values == 'criteria provided, conflicting interpretations' or \
                clinvar_info[clinvar_info.columns.values[0]].values == 'criteria provided, single submitter':
            stars = 1
        elif clinvar_info[clinvar_info.columns.values[0]].values == 'practice guideline':
            stars = 4
        elif clinvar_info[clinvar_info.columns.values[0]].values == 'reviewed by expert panel':
            stars = 3
        elif clinvar_info[
            clinvar_info.columns.values[0]].values == 'criteria provided, multiple submitters, no conflicts':
            stars = 2
        else:
            stars = 0

        stars_list = ['(']
        # filled stars
        for s in range(1, stars + 1):
            stars_list.append(html.I(className='fas fa-star'))
        # blank stars
        for t in range(1, 4 - stars + 1):
            stars_list.append(html.I(className='far fa-star'))
        stars_list.append(')')
        star = html.Span(stars_list)

        # Clinvar general information visualization
        # todo: mejorar la forma de mostrar esta parte
        clinvar_table = html.Div(
            [dbc.Row(
                [
                    dbc.Col(
                        html.Div([
                            dbc.Row(children=clinvar_info.columns.values[0],
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=clinvar_info[clinvar_info.columns.values[0]].values,
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=star,
                                    justify="center", align="center"
                                    )
                        ])
                    ),
                    dbc.Col(
                        html.Div([
                            dbc.Row(children=clinvar_info.columns.values[1],
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=clinvar_info[clinvar_info.columns.values[1]].values,
                                    justify="center", align="center"
                                    )
                        ])
                    ),
                    dbc.Col(
                        html.Div([
                            dbc.Row(children=clinvar_info.columns.values[2],
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=clinvar_info[clinvar_info.columns.values[2]].values,
                                    justify="center", align="center"
                                    )
                        ])
                    )
                ]
            ),
                html.Br(),
                dbc.Row([
                    dbc.Col(
                        html.Div([
                            dbc.Row(children=basic_info3.columns.values[0],
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=basic_info3[basic_info3.columns.values[0]].values,
                                    justify="center", align="center"
                                    )
                        ])
                    ),
                    dbc.Col(
                        html.Div([
                            dbc.Row(children='ID de NCBI',
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=var_id,
                                    justify="center", align="center"
                                    )
                        ])
                    ),
                    dbc.Col(
                        html.Div([
                            dbc.Row(children='ID de Alelo (ClinVar)',
                                    style={'fontWeight': 'bold'},
                                    justify="center", align="center"
                                    ),
                            dbc.Row(children=df_selected_variant[['CLNALLELEID']][
                                df_selected_variant[['CLNALLELEID']].columns.values[0]].values,
                                    justify="center", align="center"
                                    )
                        ])
                    )
                ])
            ])

        # clinvar_info2 = df_selected_variant['PhenotypeList'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'Phenotype'})
        # clinvar_table2 = dbc.Table.from_dataframe(clinvar_info2, striped=True, bordered=True, hover=True)

        # clinvar_info4 = df_selected_variant['PhenotypeIDS'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'References'}).replace(";", "; ", regex=True).replace(",", ", ", regex=True)
        # clinvar_table4 = dbc.Table.from_dataframe(clinvar_info4, striped=True, bordered=True, hover=True)

        # clinvar_info5 = df_selected_variant['RCVaccession'].str.split("|", expand=True).stack().to_frame().rename(columns={0:'RCV accession'})
        # clin = pd.concat([clinvar_info2, clinvar_info4, clinvar_info5], axis=1)

        clinvar_submissions = df_clinvar_submission['ReportedPhenotypeInfo'].str.split(":", n=1, expand=True)
        clinvar_sub = [0] * len(clinvar_submissions[0])
        for i in range(len(clinvar_submissions[0])):
            clinvar_sub[i] = [html.A(clinvar_submissions[1][i],
                                     href="https://www.ncbi.nlm.nih.gov/medgen/" + clinvar_submissions[0][i],
                                     target="_blank")]

        clinvar_sub = DataFrame(clinvar_sub, columns=['Reported Phenotype'])
        submitter = df_clinvar_submission[
            ['ClinicalSignificance', 'ReviewStatus', 'Submitter', 'DateLastEvaluated', 'CollectionMethod',
             'OriginCounts', 'Description']].rename(columns={'ClinicalSignificance': 'Significado Clínico',
                                                             'ReviewStatus': 'Estado de revisión',
                                                             'DateLastEvaluated': 'Última evaluación',
                                                             'CollectionMethod': 'Método de colección',
                                                             'OriginCounts': 'Origen',
                                                             'Description': 'Detalles de evidencia'})
        clin = pd.concat([clinvar_sub, submitter], axis=1)
        clinvar_table5 = dbc.Table.from_dataframe(clin, striped=True, bordered=True, hover=True,
                                                  style={'overflow': 'ellipsis', 'textOverflow': 'ellipsis'})

        clinvar_info6 = df_conflict_info[
            []]
        # clinvar_info6 = df_conflict_info.iloc[:, np.r_[3:18, 20:22]]
        clinvar_table6 = dbc.Table.from_dataframe(clinvar_info6, striped=True, bordered=True, hover=True, size='small',
                                                  style={'height': '10px', 'overflowY': 'scroll', 'overflow': 'scroll',
                                                         'textOverflow': 'ellipsis'})

        #####################################################################################################
        # Frequencies info
        #####################################################################################################

        # Show maximum frequency
        freq_m = round(df_selected_variant['freq.max'].item(), 6)
        if freq_m <= 0.001:
            col_f = px.colors.qualitative.Set1[0]
        elif 0.001 < freq_m <= 0.01:
            col_f = '#fcc323'
        elif 0.01 < freq_m <= 0.05:
            col_f = px.colors.qualitative.Vivid[2]
        elif freq_m > 0.05:
            col_f = px.colors.qualitative.Vivid[5]
        else:
            col_f = 'black'

        freq_max = html.H5(children=["Frecuencia máxima: ", html.A(freq_m, style={'color': col_f})],
                           style={'font-style': 'italic'})

        # Population Frequencies

        # European
        freq_eur = df_selected_variant[
            ['1000g2015aug_eur', 'ExAC_FIN', 'ExAC_NFE', 'gnomAD_genome_AF_fin', 'gnomAD_genome_AF_nfe',
             'esp6500siv2_ea']].rename(columns={'1000g2015aug_eur': '1000g_eur', 'gnomAD_genome_AF_fin': 'gnomAD_fin',
                                                'gnomAD_genome_AF_nfe': 'gnomAD_nfe'})
        # African
        freq_afr = df_selected_variant[
            ['1000g2015aug_afr', 'ExAC_AFR', 'gnomAD_genome_AF_afr', 'esp6500siv2_aal']].rename(
            columns={'1000g2015aug_afr': '1000g_afr', 'gnomAD_genome_AF_afr': 'gnomAD_afr',
                     'esp6500siv2_aal': 'esp6500siv2_aa'})
        # Amerindian
        freq_amr = df_selected_variant[['1000g2015aug_amr', 'ExAC_AMR', 'gnomAD_genome_AF_amr']].rename(
            columns={'gnomAD_genome_AF_amr': 'gnomAD_amr', '1000g2015aug_amr': '1000g_amr'})
        # Asian
        freq_as = df_selected_variant[
            ['1000g2015aug_eas', '1000g2015aug_sas', 'ExAC_EAS', 'ExAC_SAS', 'gnomAD_genome_AF_eas',
             'gnomAD_genome_AF_sas']].rename(columns={'1000g2015aug_eas': '1000g_eas', '1000g2015aug_sas': '1000g_sas',
                                                      'gnomAD_genome_AF_eas': 'gnomAD_eas',
                                                      'gnomAD_genome_AF_sas': 'gnomAD_sas'})
        # Other
        freq_oth = df_selected_variant[['ExAC_OTH', 'gnomAD_genome_AF_oth']]

        # Project Frequencies

        # 1000 genomes
        freq_1000g = df_selected_variant[
            ['1000g2015aug_all', '1000g2015aug_eur', '1000g2015aug_afr', '1000g2015aug_amr', '1000g2015aug_eas',
             '1000g2015aug_sas']].rename(
            columns={'1000g2015aug_all': '1000g_all', '1000g2015aug_eur': '1000g_eur', '1000g2015aug_afr': '1000g_afr',
                     '1000g2015aug_eas': '1000g_eas', '1000g2015aug_sas': '1000g_sas', '1000g2015aug_amr': '1000g_amr'})
        # ExAC
        freq_exac = df_selected_variant[
            ['ExAC_ALL', 'ExAC_FIN', 'ExAC_NFE', 'ExAC_AFR', 'ExAC_AMR', 'ExAC_EAS', 'ExAC_SAS', 'ExAC_OTH']]
        # gnomAD
        freq_gnomad = df_selected_variant[
            ['gnomAD_genome_AF', 'gnomAD_genome_AF_fin', 'gnomAD_genome_AF_nfe', 'gnomAD_genome_AF_afr',
             'gnomAD_genome_AF_amr', 'gnomAD_genome_AF_eas', 'gnomAD_genome_AF_sas',
             'gnomAD_genome_AF_oth']].rename(columns={'gnomAD_genome_AF': 'gnomAD',
                                                      'gnomAD_genome_AF_afr': 'gnomAD_afr',
                                                      'gnomAD_genome_AF_fin': 'gnomAD_fin',
                                                      'gnomAD_genome_AF_nfe': 'gnomAD_nfe',
                                                      'gnomAD_genome_AF_amr': 'gnomAD_amr',
                                                      'gnomAD_genome_AF_eas': 'gnomAD_eas',
                                                      'gnomAD_genome_AF_sas': 'gnomAD_sas',
                                                      'gnomAD_genome_AF_oth': 'gnomAD_oth'})

        freq_esp = df_selected_variant[['esp6500siv2_all', 'esp6500siv2_aal', 'esp6500siv2_ea']].rename(
            columns={'esp6500siv2_aal': 'esp6500siv2_aa'})

        # freq_info = df_selected_variant[['esp6500siv2_all', '1000g2015aug_all', 'ExAC_ALL']]
        # freq_table = dbc.Table.from_dataframe(freq_info, striped=True, bordered=True, hover=True)

        names = [freq_eur, freq_afr, freq_amr, freq_as, freq_1000g, freq_exac, freq_gnomad, freq_esp]
        titles = ['European', 'African', 'Amerindian', 'Asian', '1000 Genomes', 'ExAC', 'gnomAD', 'ESP 6500']
        freq_table = [0] * len(names)
        for i in range(len(names)):
            color_bar = [0] * len(names[i].values[0])
            for j in range(len(names[i].values[0])):
                if names[i].values[0][j] < 0.001:
                    color_bar[j] = px.colors.qualitative.Set1[0]
                elif 0.001 <= names[i].values[0][j] < 0.01:
                    color_bar[j] = '#fcc323'
                elif 0.01 <= names[i].values[0][j] < 0.05:
                    color_bar[j] = px.colors.qualitative.Vivid[2]
                elif names[i].values[0][j] >= 0.05:
                    color_bar[j] = px.colors.qualitative.Vivid[5]
                else:
                    color_bar[j] = 'black'
            data = [go.Bar(x=names[i].values[0], y=names[i].columns.values, orientation='h', width=0.3,
                           marker={'color': color_bar})]
            layout = go.Layout(title=titles[i], title_x=0.5, title_y=0.85, xaxis=dict(range=[0, freq_m]),
                               xaxis_title="Frecuencia")
            freq_table1 = go.Figure(data=data, layout=layout)
            # freq_table[i] = dcc.Graph(figure= {'data':[{'x':names[i].values[0], 'y':names[i].columns.values}], 'type':'bar'})
            freq_table[i] = dcc.Graph(figure=freq_table1, style={'height': 400}, config={'displayModeBar': False})

            #####################################################################################################
            # Predictors
            #####################################################################################################
            # TODO: add information about each score (and links)
            # TODO: create function to transform predictions

            # GRANTHAM SCORE
            # ----------------------------------------------------------------------------------------------------
            grantham_pred = np.NaN
            grant = df_selected_variant['grantham_score'][0]
            if grant <= 50:
                grantham_pred = 'Conservado'
            elif 51 <= grant < 101:
                grantham_pred = 'Moderadamente conservado'
            elif 101 <= grant < 151:
                grantham_pred = 'Moderadamente radical'
            elif grant >= 151:
                grantham_pred = 'Radical'

            grantham = [html.A(html.Span(["Grantham Score ", html.I(className='fas fa-external-link-alt')]),
                               href="https://pubmed.ncbi.nlm.nih.gov/4843792/", target="_blank"), grant, grantham_pred]

            # SIFT
            # ----------------------------------------------------------------------------------------------------
            sift_pred = np.NaN
            sift_score = df_selected_variant['SIFT_pred'][0]
            if sift_score == "T":
                sift_pred = "Tolerado"
            elif sift_score == "D":
                sift_pred = "Dañino"

            sift = [html.A(html.Span(["SIFT ", html.I(className='fas fa-external-link-alt')]),
                           href="https://sift.bii.a-star.edu.sg/", target="_blank"),
                    round(df_selected_variant['SIFT_score'][0], 2), sift_pred]

            # LRT
            # ----------------------------------------------------------------------------------------------------
            lrt_pred = np.NaN
            lrt_score = df_selected_variant['LRT_pred'][0]
            if lrt_score == "D":
                lrt_pred = "Deletéreo"
            elif lrt_score == "N":
                lrt_pred = "Neutral"
            elif lrt_score == "U":
                lrt_pred = "Desconocido"

            lrt = [html.A(html.Span(["LRT ", html.I(className='fas fa-external-link-alt')]),
                          href="https://genome.cshlp.org/content/19/9/1553.short", target="_blank"),
                   round(df_selected_variant['LRT_score'][0], 2), lrt_pred]

            # Mutation Taster
            # ----------------------------------------------------------------------------------------------------
            mut_pred = np.NaN
            mut_score = df_selected_variant['MutationTaster_pred'][0]
            if mut_score == "A":
                mut_pred = "Causante de enfermedad automático"
            elif mut_score == "D":
                mut_pred = "Causante de enfermedad"
            elif mut_score == "P":
                mut_pred = "Polimorfismo automático"
            elif mut_score == "N":
                mut_pred = "Polimorfismo"

            mut_taster = [html.A(html.Span(["Mutation Taster ", html.I(className='fas fa-external-link-alt')]),
                                 href="https://www.mutationtaster.org/", target="_blank"),
                          round(df_selected_variant['MutationTaster_score'][0], 2), mut_pred]

            # PolyPhen-2
            # ----------------------------------------------------------------------------------------------------

            polyphen_2_hvar = [html.A(html.Span(['PolyPhen2 HVAR ', html.I(className='fas fa-external-link-alt')]),
                                      href="http://genetics.bwh.harvard.edu/pph2/", target="_blank"),
                               round(df_selected_variant['Polyphen2_HVAR_score'][0], 2),
                               common.to_pred_polyphen(df_selected_variant['Polyphen2_HVAR_pred'][0])]
            polyphen_2_div = [html.A(html.Span(['PolyPhen2 HDIV ', html.I(className='fas fa-external-link-alt')]),
                                     href="http://genetics.bwh.harvard.edu/pph2/", target="_blank"),
                              round(df_selected_variant['Polyphen2_HDIV_score'][0], 2),
                              common.to_pred_polyphen(df_selected_variant['Polyphen2_HDIV_pred'][0])]

            # CADD
            # ----------------------------------------------------------------------------------------------------
            cadd_phred = round(df_selected_variant['CADD_phred'][0], 2)
            if cadd_phred <= 20:
                cadd_pred = 'Tolerado (mayor score, mayor patogenicidad)'
            elif cadd_phred > 20:
                cadd_pred = 'Deletéreo (mayor score, mayor patogenicidad)'
            else:
                cadd_pred = 'Desconocido'
            cadd = [html.A(html.Span(['CADD (Phred) ', html.I(className='fas fa-external-link-alt')]),
                           href="https://cadd.gs.washington.edu/", target="_blank"), cadd_phred, cadd_pred]

            # Construct the table with physicochemical impact predictors
            pred_names = ["Predictor", "Score", "Predicción"]
            predict = [grantham, sift, lrt, mut_taster, polyphen_2_hvar, polyphen_2_div, cadd]
            df = pd.DataFrame(predict, columns=pred_names)

            for i in range(len(df["Predicción"])):
                df["Predicción"][i] = html.Span(df["Predicción"][i],
                                                style={"color": common.color_prediction(df["Predicción"][i])})

            # sc = df_selected_variant[['grantham_score', 'SIFT_score', 'SIFT_pred', 'CADD_raw_rankscore', 'LRT_score']]
            sc_table1 = dbc.Table.from_dataframe(df, style={'text-align': 'center'})

            # PhyloP
            # ----------------------------------------------------------------------------------------------------

            phylop100 = [html.A(html.Span(['phyloP 100 Way Vertebrate ', html.I(className='fas fa-external-link-alt')]),
                                href="http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phyloP17way/", target="_blank"),
                         round(df_selected_variant['phyloP100way_vertebrate'][0], 2),
                         common.get_prediction(df_selected_variant['phyloP100way_vertebrate'][0], 'phyloP')]

            phylop20 = [html.A(html.Span(['phyloP 20 Way Mammalian ', html.I(className='fas fa-external-link-alt')]),
                               href="http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phyloP30way/", target="_blank"),
                        round(df_selected_variant['phyloP20way_mammalian'][0], 2),
                        common.get_prediction(df_selected_variant['phyloP20way_mammalian'][0], 'phyloP')]

            # Siphy
            # ----------------------------------------------------------------------------------------------------
            siphy = [html.A(html.Span(['SiPhy (29-way) log Odds ', html.I(className='fas fa-external-link-alt')]),
                            href="https://www.broadinstitute.org/mammals-models/29-mammals-project-supplementary-info",
                            target="_blank"), round(df_selected_variant['SiPhy_29way_logOdds'][0], 2),
                     common.get_prediction(df_selected_variant['SiPhy_29way_logOdds'][0], 'SiPhy')]

            # GERP
            # ----------------------------------------------------------------------------------------------------
            gerp = [html.A(html.Span(['GERP ', html.I(className='fas fa-external-link-alt')]),
                           href="http://mendel.stanford.edu/SidowLab/downloads/gerp/index.html", target="_blank"),
                    round(df_selected_variant['GERP++_RS'][0], 2),
                    common.get_prediction(df_selected_variant['GERP++_RS'][0], 'GERP')]

            # Construct the table with evolutive conservation predictors
            pred_names = ["Predictor", "Score", "Predicción"]
            predict = [phylop100, phylop20, siphy, gerp]
            df = pd.DataFrame(predict, columns=pred_names)
            for i in range(len(df["Predicción"])):
                df["Predicción"][i] = html.Span(df["Predicción"][i],
                                                style={"color": common.color_prediction(df["Predicción"][i])})

            sc_table2 = dbc.Table.from_dataframe(df, style={'text-align': 'center'})

            #####################################################################################################
            # Sample
            #####################################################################################################

            if pd.isna(df_selected_variant['patient'][0]):
                df1 = pd.DataFrame(columns=['Fenotipo', 'Herencia', 'Cigosidad', 'Sexo'])
                df1 = df1.append({'Fenotipo': '-', 'Herencia': '-', 'Cigosidad': '-', 'Sexo': '-'}, ignore_index=True)
                sample_table_1 = dbc.Table.from_dataframe(df1)
                df2 = pd.DataFrame(columns=['Familia afectada', 'Etnicidad', 'Origen de la muestra'])
                df2 = df2.append({'Familia afectada': '-', 'Etnicidad': '-', 'Origen de la muestra': '-'},
                                 ignore_index=True)
                sample_table_2 = dbc.Table.from_dataframe(df2)


            else:
                print('hola', df_selected_variant['patient'][0])
                api_route_sample = '/sample/' + df_selected_variant['patient'][0]
                status_sample, df_sample = common.api_request(api_route_sample, 'GET')

                sample_info = df_sample[['phenotype', 'inheritance', 'zigocity', 'sex']]
                sample_info = sample_info.rename(
                    columns={'phenotype': 'Fenotipo', 'inheritance': 'Herencia', 'zigocity': 'Cigosidad',
                             'sex': 'Sexo'})
                sample_table_1 = dbc.Table.from_dataframe(sample_info)

                sample_info = df_sample[['family_affected', 'ethnicity', 'origin']]
                sample_info = sample_info.rename(
                    columns={'family_affected': 'Familia afectada', 'ethnicity': 'Etnicidad',
                             'origin': 'Origen de la muestra'})
                sample_table_2 = dbc.Table.from_dataframe(sample_info)

        options_default_value = ""  # allows to reset the dropdowns to the default value

        return [title, is_acmg, tt, qual, dp, basic_table, basic_table2, basic_table3, button_omim, omim_table, omim_table_2, button_clinvar, name_clinvar, clinvar_table,  clinvar_table5, clinvar_table6,
                "tab_freq_1", freq_max, freq_table[0], freq_table[1], freq_table[2], freq_table[3], freq_table[4], freq_table[5],
                freq_table[6], freq_table[7],sc_table1, sc_table2, sc_table2, sample_table_1, sample_table_2, "tab_1", {'display': 'block'}, options_default_value]
    else:
        raise dash.exceptions.PreventUpdate  # avoid update of elements


@app.callback(
    [
        Output('msg-classification-feedback', 'children'),
        Output('msg-classification-feedback', 'color'),
        Output('msg-classification-feedback', 'style'),
        Output('rules_mate', 'children'),
        Output('table_rules', 'children'),

    ],
    [
        Input('btn-submit-classification', 'n_clicks'),
        Input('combo-labels', 'value')
    ]
)
def show_training_feedback(n_clicks, selected_label):
    """
    Show feedback based on user classification
    :param n_clicks:
    :param selected_label: selected value in classification combobox
    :return: [string, string, style]
    """
    context_id = common.get_trigger_id()

    if context_id == 'btn-submit-classification':
        global expected_label
        global selected_variant_id



        print('\tUser says:', selected_label, ', Expected:', expected_label)
        if selected_label.lower() == str(expected_label).lower():  # convert to lower to avoid lower/uppercase problems
            # User answer is correct
            is_correct = 1
        else:
            # User answer is wrong
            is_correct = 0

        # send classification data to API
        route = '/users/training/set'
        method = 'POST'
        data = {'user_id': current_user.user, 'variant_id': selected_variant_id, 'label_id': selected_label,
                'is_correct': is_correct}
        status, response = common.api_request(route, method, data, False)

        route_update = '/users/points/training'
        data_update = {'user_id': current_user.user, 'is_correct': is_correct}
        status_update, response_update = common.api_request(route_update, method, data_update, False)

        if status == 'ok':
            if is_correct == 1:
                msg = "Muy bien, etiqueta correcta. ¡Felicitaciones!"
                color = "success"
            else:
                msg = [html.Span("Clasificación incorrecta, la etiqueta correcta es: "), html.Strong(expected_label)]
                color = "danger"
        else:
            if response == 'IntegrityError':
                # variant already classified
                msg = html.Span("You've already classified variant " + str(selected_variant_id))
                color = "warning"
            elif response == 'APIError':
                # API connection error
                msg = [html.Strong("Error de conexión de la API:"), html.Span(" la clasificación no pudo ser guardada.")]
                color = "danger"
            else:
                msg = [html.Strong("Error interno:"), html.Span(" la clasificación no pudo ser guardada.")]
                color = "danger"

        style = {'display': 'block'}

        status, selected_variant = common.api_request('/variants/'+str(selected_variant_id), 'GET', True)



        button_acmg = html.A(html.Span([html.I(className='fas fa-external-link-alt'), " Interpretación"]),
                             href="https://www.nature.com/articles/gim201530.pdf", target="_blank",
                             style={"font-size": "14px"})

        variant_rules = selected_variant.iloc[:, 156:183].T.reset_index()
        variant_rules.columns = ['rule', 'value']
        result = pd.merge(variant_rules, df_rules, on=["rule", "rule"])
        result["color"] = np.nan
        result.at[:4, ["color"]] = "#c73537"
        result.at[5:10, ["color"]] = "#d77b56"
        result.at[11:15, ["color"]] = "#708846"
        result.at[16:20, ["color"]] = "#623a7d"
        result.at[21:26, ["color"]] = "#5080ad"
        filtered = result.query('value == 1')['rule'].to_list()

        final_rules = html.A(html.Span([html.I("Reglas que se cumplen: " + ", ".join(filtered))],
                                      style={"font-size": "18px", "color": "black"}))

        table_header = [
            html.Thead(html.Tr([html.Th("Regla"), html.Th("Explicación")]))
        ]
        a = []
        for index, row in result.iterrows():
            row = html.Tr([html.Td(row['rule']), html.Td(row['detailed_rule'])],
                          style={'background-color': row['color'], 'color': 'white'}) if (row['value'] == 1) else (
                html.Tr([html.Td(row['rule'], style={'color': row['color']}), html.Td(row['detailed_rule'])])) if (
                    row['value'] == 0) else (html.Tr([html.Td(row['rule']), html.Td(row['detailed_rule'])],
                                                     style={'background-color': "#cccccc", 'color': 'white'}))
            a.append(row)
        table_body = [html.Tbody(a)]
        tabla_reglas = dbc.Table(table_header + table_body, bordered=False)

        coll_2 = html.Div([
            dbc.Row([html.A(html.Span([html.I(className='fas fa-newspaper'),
                                       " Standards and guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College of Medical Genetics and Genomics and the Association for Molecular Pathology"]),
                            href="https://www.nature.com/articles/gim201530.pdf", target="_blank")
                     ]),
            dbc.Row([html.A(html.Span([html.I(className='fas fa-newspaper'),
                                       " Sherloc: a comprehensive refinement of the ACMG-AMP variant classification criteria"]),
                            href="https://www.gimjournal.org/article/S1098-3600(21)01370-8/pdf", target="_blank")
                     ])
        ])


    else:
        msg = ''
        color = ''
        style = {'display': 'none'}
        final_rules = ''
        tabla_reglas = ''


    return [msg, color, style, final_rules, tabla_reglas]
