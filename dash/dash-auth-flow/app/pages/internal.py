import dash
import dash_table
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output,Input,State
import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import numpy as np
from pandas import DataFrame
from flask_login import logout_user, current_user
from dash_extensions import Download
from dash_extensions.snippets import send_data_frame
import re
import base64
import datetime
import io
import config
import common
from server import app

login_alert = dbc.Alert(
    'User not logged in. Taking you to login.',
    color='danger'
)

colors = {
    'background': '#FFFFFF',
    'background_2': '#e5f5f2',
    'background_3': '#D0E3F5',
    'text_1': '#74807E',
    'text_2': '#008080',
    'text_3': '#FF5733',
    'text_4': '#8E7F7A',
    'text_5': '#193176',
    'text_6': '#45B39D',
    'text_7': '#202a3d',
    'pills_1': '#E5E5E5',
    'conflicting':'#F77910'}

tab_selected_style = {
    'borderTop': '3px solid #E36209 !important'
}

location_int = dcc.Location(id='page3-url', refresh=True)

def layout():
    return dbc.Row(
        dbc.Col(
            [
                location_int,
                dbc.Card(
                    dbc.CardBody([
                        html.H2("Clasificación interna", style={'color': colors['text_7']}),
                        html.Hr(className="my-2"),
                        html.Div([
                            dbc.Button(
                                children=html.Span([html.I(className='fas fa-info-circle'), ' Más información']),
                                id='open', color="primary"),
                            dbc.Modal([
                                dbc.ModalHeader("Clasificación interna"),
                                dbc.ModalBody("Esta página se brinda a modo de servicio, para aportar una etiqueta a una variante ingresada desde nuestro predictor."),
                                dbc.ModalFooter(
                                    dbc.Button("Close", id="close", className="ml-auto")
                                ),
                            ],
                                id="modal",
                            )
                        ]),
                    ]), color="light"
                ),
                html.Br(),
                html.Br(),
                html.Div([
                    dcc.Upload(
                        id='upload-data',
                        children=html.Div([
                            'Arrastrar y soltar  ',
                            html.A('Seleccionar archivo')
                        ]),
                        style={
                            'width': '100%',
                            'height': '60px',
                            'lineHeight': '60px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },
                        # Allow multiple files to be uploaded
                        multiple=True
                    ),
                    html.Div(id='output-data-upload'),
                ]),
                html.Br(),
                html.Br(),
                html.Br(),
                dbc.Card([
                    dbc.CardHeader(children=[
                        dbc.Row([
                            dbc.Col([
                                html.Div("Predicción")
                            ])
                        ])
                    ], style={'fontWeight': 'normal', 'color': colors['text_1'], 'font-size': '21px'}),
                    dbc.CardBody([
                        dbc.Row([
                            dbc.Col([
                                html.Div(id='var', children=[
                                    html.H6('Variante: '),
                                ]),
                                html.Div(id='pred', children=[
                                    html.H6('Predicción: ')
                            ])
                        ])
                    ])
                ])
                ])]
        ))

def parse_contents(contents, filename, date):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
        elif 'tsv' in filename:
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')), sep='\t')
    except Exception as e:
        print(e)
        return html.Div([
            'Hubo un error procesando el archivo.'
        ])

    return html.Div([
        html.H5(filename),
        html.H6(datetime.datetime.fromtimestamp(date)),

        dash_table.DataTable(
            df.to_dict('records'),
            [{'name': i, 'id': i} for i in df.columns]
        ),

        html.Hr(),  # horizontal line

        # For debugging, display the raw contents provided by the web browser
        html.Div('Raw Content'),
        html.Pre(contents[0:200] + '...', style={
            'whiteSpace': 'pre-wrap',
            'wordBreak': 'break-all'
        })
    ])

#@app.callback([Output('output-data-upload', 'children')],
#              [Input('upload-data', 'contents')],
#              [State('upload-data', 'filename'),
#              State('upload-data', 'last_modified')])

#def update_output(list_of_contents, list_of_names, list_of_dates):
#    if list_of_contents is not None:
#        children = [
#            parse_contents(c, n, d) for c, n, d in
#            zip(list_of_contents, list_of_names, list_of_dates)]
#        return children
