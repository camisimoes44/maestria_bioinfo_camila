# Dash app initialization
import dash
import dash_bootstrap_components as dbc

# global imports
from flask_login import LoginManager, UserMixin

# local imports
from utilities.auth import db, User as base
from utilities.config import config, engine
import common
BS = "https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/flatly/bootstrap.min.css"

FONT_AWESOME = "https://use.fontawesome.com/releases/v5.15.1/css/all.css"
app = dash.Dash(
    __name__,
    external_stylesheets=[BS, FONT_AWESOME]
)
#dbc.themes.BOOTSTRAP
server = app.server
app.config.suppress_callback_exceptions = True
# app.css.config.serve_locally = True
# app.scripts.config.serve_locally = True
app.title = 'Bioinformatics'

# config
server.config.update(
    SECRET_KEY='make this key random or hard to guess',
    SQLALCHEMY_DATABASE_URI=config.get('database', 'con'),
    SQLALCHEMY_TRACK_MODIFICATIONS=False
)

db.init_app(server)

# Setup the LoginManager for the server
login_manager = LoginManager()
login_manager.init_app(server)
login_manager.login_view = '/login'

curr_logged_users = dict()  # dictionary of User objects instances for current logged in users


# Create User class with UserMixin
class User(UserMixin, base):
    def __init__(self, user):
        self.id = user['user']
        self.user = user['user']
        self.first = user['first']
        self.last = user['last']
        self.email = user['email']
        self.level = user['level']
        self.logged_since = common.get_current_datetime()
        # self.password = user['password']
        self.access_level = user['access_level']
        self.is_active = True
        self.is_authenticated = True
        self.is_anonymous = False  # anonymous users are not allowed

        global curr_logged_users
        curr_logged_users[self.user] = self  # add current instance to the dict of logged users

    def is_active(self):
        return self.is_active

    def is_authenticated(self):
        return self.is_authenticated

    def is_anonymous(self):
        return self.is_anonymous

    def get_id(self):
        return self.id


# instantiate an empty (not logged in) user
usr_data = {'user': 'None', 'first': 'None', 'last': 'None', 'email': 'None', 'level': 'None', 'access_level': 0}
usr_not_logged = User(usr_data)
usr_not_logged.is_active = False
usr_not_logged.is_authenticated = False


# callback to reload the user object
@login_manager.user_loader
def load_user(user_id):
    # search if user is currently logged in
    if user_id in curr_logged_users:
        curr_user = curr_logged_users[user_id]
        print('\t> Load user:', str(user_id), '--> logged in since', curr_user.logged_since)
        return curr_user
    else:
        print('\t> Load user:', str(user_id), '--> NOT logged in')
        return usr_not_logged

    # return User({'user': user_id})
