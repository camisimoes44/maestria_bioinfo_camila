import dash
import pandas as pd
import requests
import json
import config
import datetime
import numpy as np
import base64
import dash_html_components as html
import io


def api_request(api_route, method='GET', data=None, convert_to_df=True):
    """
    Perform a request to the given API route

    :param api_route: api route to send request (string)
    :param method: request method
    :param data: data to append to request (dict)
    :param convert_to_df: convert response (boolean). True=Pandas DataFrame, False=JSON
    :return: response (Pandas DataFrame/JSON)
    """
    if data is None:
        data = {}
    api_url = config.api_url + api_route

    try:
        response = None
        if method.upper() == 'GET':
            response = requests.get(api_url)
        elif method.upper() == 'POST':
            response = requests.post(api_url, data=data)
    except requests.exceptions.ConnectionError:
        print("ERROR: Couldn't connect to API. Please try again.")
        return 'error', "APIError"

    if response.status_code == 200:
        decoded_response = response.content.decode('utf-8')
        response_status = json.loads(decoded_response)['status']
        response_data = json.loads(decoded_response)['data']
        if convert_to_df:
            # data_df = pd.DataFrame.from_dict(json.loads(decoded_response), orient='index')
            try:
                response_data_df = pd.read_json(response_data)
            except ValueError:
                # replace of '://' in case of exception thrown by fsspec library
                # with paths including that sequence of characters
                response_data = response_data.replace('://', ': //')
                response_data_df = pd.read_json(response_data)
            return response_status, response_data_df
        else:
            return response_status, response_data
    else:
        return 'error', 'None'


def get_trigger_id():
    """
    Get the element id that triggered a callback

    :return: element id (string)
    """
    context = dash.callback_context
    if context.triggered:
        context_id = context.triggered[0]['prop_id'].split('.')[0]
    else:
        context_id = ''
    print('> trigger:', context_id)
    return context_id


def get_current_datetime():
    """
    Get the current datetime with format dd/mm/yy HH:MM:SS.ms

    :return: (string) current datetime
    """
    now = datetime.datetime.now()
    return now.strftime("%D %H:%M:%S.%f")[:-3]


def get_prediction(score, predictor):
    """
    Get the pathogenicity score

    :return: (string) prediction
    """

    if predictor == 'phyloP':
        if score <= 0:
            prediction = 'Poco conservado'
        elif score > 0:
            prediction = 'Conservado'
        else:
            prediction = 'N/A'
        return prediction
    elif predictor == 'SiPhy':
        if score <= 12.17:
            prediction = 'Poco conservado'
        elif score > 12.17:
            prediction = 'Conservado'
        else:
            prediction = 'N/A'
        return prediction
    elif predictor == 'GERP':
        if score <= 2:
            prediction = 'Poco conservado'
        elif score > 2:
            prediction = 'Conservado'
        else:
            prediction = 'N/A'
        return prediction
    else:
        return 'error'


def to_pred_polyphen(poly_score):
    poly_pred = np.NaN
    if poly_score == "D":
        poly_pred = "Probablemente Dañino"
    elif poly_score == "P":
        poly_pred = "Posiblemente Dañino"
    elif poly_score == "B":
        poly_pred = "Benigno"
    return poly_pred

def color_prediction(prediction):
    color_pred = 'grey'
    if prediction in ['Tolerado','Tolerado (mayor score, mayor patogenicidad)', 'Polimorfismo', 'Polimorfismo automático', 'Neutral', 'Radical', 'Moderadamente radical', 'Poco conservado', 'Benigno', 'Conservativo', 'Moderadamente conservativo']:
        color_pred = "#5fb06f"
    elif prediction in ['Deletéreo', 'Causante de enfermedad', 'Causante de enfermedad automático', 'Dañino', 'Conservado', 'Moderadamente conservado', 'Probablemente Dañino', 'Posiblemente Dañino', 'Deletéreo (mayor score, mayor patogenicidad)']:
        color_pred = "#b01909"
    return color_pred


def parse_contents(contents, filename):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'tsv' in filename:
            # Assume that the user uploaded a TSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')), sep ='\t')
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])
    return df
