# Web application configuration

# Development setup
server_ip = '0.0.0.0'  # (string)
server_port = 8050  # (int)
debug = True  # (boolean)

api_url = 'http://127.0.0.1:5000/api/v1'  # (string)

clinvar_base_url = 'https://www.ncbi.nlm.nih.gov/clinvar/variation/'
omim_base_url = 'https://www.omim.org/entry/'
