--
-- Dumping data for table `comments`
--

-- Dumping data for table `expertise_levels`
LOCK TABLES `expertise_levels` WRITE;
/*!40000 ALTER TABLE `expertise_levels` DISABLE KEYS */;
INSERT INTO `expertise_levels` VALUES ('Principiante',50),('Competente',150),('Experto',400),('Novato',0),('Avanzado',250);
/*!40000 ALTER TABLE `expertise_levels` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Considerada benigna o sin significado clínico (incluye benignas y probablemente benignas, pero también VUS en heterocigosis para fenotipos recesivos)'),(2,'Considerada benigna por ser intrónica (tiene frecuencia baja, pero no hay más datos que indiquen otra cosa)'),(3,'VUS (en genes incidentales reportables o potencialmente relacionados al fenotipo)'),(4,'No corresponde al fenotipo en estudio (y no se reportaría)'),(5,'No fenotipo o riesgo conocido asociado al gen'),(6,'Heterocigota en AR o XR (XX) (probablemente patogénicas o patogénicas, ocasionalmente VUS, reportables al estar potencialmente vinculadas al fenotipo o como riesgo reproductivo)'),(7,'Incidental (probablemente patogénicas o patogénicas no relacionadas al fenotipo en estudio, pero de relevancia clínica y que se piensa reportar)'),(8,'Candidata (probablemente patogénicas o patogénicas, ocasionalmente VUS, relacionadas al fenotipo en estudio)');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


-- Dumping data for table `rules`
LOCK TABLES `rules` WRITE;
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` VALUES ('PVS1','Null variant(nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single o multiexon deletion) en un gen en el que la LOF es un mecanismo conocido de enfermedad'),('PS1','Mismo cambio de aminoácido que una variante patógena previamente establecida, independientemente del cambio de nucleótido'),('PS2','De novo (tanto maternidad como paternidad confirmadas) en un paciente con la enfermedad y sin antecedentes familiares'),('PS3','Estudios funcionales bien establecidos in vitro o in vivo que apoyen un efecto perjudicial sobre el gen o el producto génico'),('PS4','La prevalencia de la variante en los individuos afectados aumenta significativamente en comparación con la prevalencia en los controles'),('PM1','Situada en un hot spot mutacional y/o dominio funcional crítico y bien establecido (por ejemplo, el sitio activo de una enzima) sin variación benigna'),('PM2','Ausente en los controles (o con una frecuencia extremadamente baja si es recesivo) (Tabla 6) en Exome Sequencing Project, 1000 Genomes Project o Exome Aggregation Consortium'),('PM3','Para trastornos recesivos, detectados en trans con una variante patogénica'),('PM4','Cambios en la longitud de la proteína como resultado de deleciones/inserciones dentro del marco en una región no repetida o variantes stop-loss'),('PM5','Variante nueva missense en un residuo de aminoácido en el que se ha observado anteriormente una variante missense diferente determinada como patogénica'),('PM6','Asunción de novo, pero sin confirmación de paternidad y maternidad'),('PP1','Cosegregación con enfermedad en múltiples familiares afectados en un gen definitivamente conocido como causante de la enfermedad'),('PP2','Variante missense en un gen que tiene una baja tasa de variación missense benigna y en el que las variantes missense son un mecanismo común de enfermedad'),('PP3','Múltiples líneas de evidencia computacional apoyan un efecto deletéreo en el gen o producto génico (conservación, evolución, impacto de splicing, etc.)'),('PP4','El fenotipo del paciente o los antecedentes familiares son muy específicos de una enfermedad con una única etiología genética'),('PP5','Fuente reputada informa recientemente de que la variante es patogénica, pero el laboratorio no dispone de pruebas para realizar una evaluación independiente'),('BA1','La frecuencia alélica es >5% en Exome Sequencing Project, 1000 Genomes Project, o Exome Aggregation Consortium'),('BS1','La frecuencia alélica es mayor de lo esperado para el trastorno'),('BS2','Observado en un individuo adulto sano para un trastorno recesivo (homocigoto), dominante (heterocigoto) o ligado al cromosoma X (hemicigoto), con penetrancia completa esperada a una edad temprana'),('BS3','Estudios funcionales bien establecidos in vitro o in vivo no muestran ningún efecto perjudicial sobre la función o el splicing de las proteínas'),('BS4','Falta de segregación en los miembros afectados de una familia'),('BP1','Variante missense en un gen del que se sabe que las variantes principalmente truncantes causan enfermedades'),('BP2','Observado en trans con una variante patogénica para un gen/trastorno dominante totalmente penetrante u observado en cis con una variante patogénica en cualquier patrón de herencia'),('BP3','Deleciones/inserciones in-frame en una región repetitiva sin función conocida'),('BP4','Múltiples líneas de evidencia computacional sugieren que no hay impacto en el gen o producto génico (conservación, evolución, impacto de splicing, etc.)'),('BP5','Variante encontrada en un caso con una base molecular alternativa para la enfermedad'),('BP6','Una fuente reputada informa recientemente de que la variante es benigna, pero el laboratorio no dispone de pruebas para realizar una evaluación independiente'),('BP7','Una variante sinónima (silent) para la que los algoritmos de predicción de splicing no predicen ningún impacto en la secuencia consenso de empalme ni la creación de un nuevo sitio de empalme Y el nucleótido no está altamente conservado');
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;
UNLOCK TABLES;


-- Dumping data for table `labels`
LOCK TABLES `labels` WRITE;
/*!40000 ALTER TABLE `labels` DISABLE KEYS */;
INSERT INTO `labels` VALUES ('Benign',1,1),('Conflicting',0,0),('Likely Benign',1,1),('Likely Pathogenic',1,1),('Pathogenic',1,1),('Response to drugs',1,1),('Risk factor',1,1),('VUS',1,1);
/*!40000 ALTER TABLE `labels` ENABLE KEYS */;
UNLOCK TABLES;


-- Dumping data for table `labels`
LOCK TABLES `users_organization` WRITE;
/*!40000 ALTER TABLE `labels` DISABLE KEYS */;
INSERT INTO `users_organization` VALUES ('Institut Pasteur de Montevideo',1,NULL,'Mataojo 2020, Montevideo','Uruguay',NULL,NULL,NULL,NULL,NULL),('CENUR Litoral Norte, Ingenieria Biologica',2,NULL,'Ruta 3 Km 363, Paysandú','Uruguay',NULL,NULL,NULL,NULL,NULL),('Facultad de Medicina',3,NULL,'Av. Gral. Flores 2125, Montevideo','Uruguay',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `labels` ENABLE KEYS */;
UNLOCK TABLES;


-- Dumping data for table `users`

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('abatalla','Ana','Batalla','nyma72@hotmail.com','Competente','$5$rounds=535000$HsEEdiFPh5nJGHjZ$uvwmoYFRxF6h5jtJZLCscJW6WoeR/r..dpD.ROAT2hB',3,1,150,NULL,NULL,NULL,NULL,NULL,NULL),('blawlor','Bárbara','Lawlor','drabarbaralawlor@gmail.com','Principiante','$5$rounds=535000$QdLuBe4PdsX25oca$.fUTNESRCngo.htEGm740ArcsdPfsEl4nzQHOSMdA36',3,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('cblanc','Carolina','Blanc','blanc39.55@gmail.com','Avanzado','$5$rounds=535000$s3uaCTPkIkaqDtMi$qw3bmdIPa7Fd0L9r2V.5mrC/azSdJXty2CsfMYviI36',3,2,250,NULL,NULL,NULL,NULL,NULL,NULL),('csimoes','Camila','Simoes','csimoes@cup.edu.uy','Principiante','$5$rounds=535000$3Gb36zWPuZLpr67C$8VOf22akihE/IhLVAEFnjbQrD9ZeCG789t1N51lstQ8',2,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('ljimenez','Lucía','Jimenez','lujimenezsol@gmail.com','Principiante','$5$rounds=535000$bdugM.C1Dv4tEpG9$v269EhNNZOPYJCVAUXP0uHn5Gs/1Z5X3FExuHyIi365',3,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('lrodriguez','Laura','Rodriguez','laura2r@yahoo.com','Principiante','$5$rounds=535000$uuuVLCCcYvsEU3xi$.v64skJVDF2rFoOpyGzZYVsQkHvCDzanxijieOs9Bt8',3,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('marevalo','Martín','Arévalo','marevalo@cup.edu.uy','Novato','$5$rounds=535000$fZ9cRITgpdIInXlk$4RhD4rfWTcQyrXIt7z9xB8T.oC1Z2CdX4BzFG2YTnX/',2,1,0,NULL,NULL,NULL,NULL,NULL,NULL),('mmello','María','Mello','mdemello1@hotmail.com','Principiante','$5$rounds=535000$esnfoWQmkrz9HSx5$zEu0NEpFYcFCSS4w51arqhVoAxAsdM.IonApfS/30O8',3,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('mmoraes','Mariana','Moraes','marianamoraes_16@hotmail.com','Principiante','$5$rounds=535000$A/gHcfusTcyopRN/$5dsKo3ofYTe/l6W4felA0sIaYDuQAKlf8ZYoyQJJKA3',3,1,50,NULL,NULL,NULL,NULL,NULL,NULL),('ndelloca','Nicolás','Dell´Oca','ndellocaru@gmail.com','Avanzado','$5$rounds=535000$LqG0405Vy6sYd7ug$xCkIq8mEamc1n0cYBXJ4o5FQ7jiocq1Qbndu2br2pE4',3,2,250,NULL,NULL,NULL,NULL,NULL,NULL),('srodriguez','Soledad','Rodriguez','soledadrmb@gmail.com','Experto','$5$rounds=535000$2n4n8EK9CsuDgr/p$sWGGnJbUPE15/aPnbUNt5hoUq1Y8YfjS.V0cj.StSE/',3,3,400,NULL,NULL,NULL,NULL,NULL,NULL),('vraggio','Víctor','Raggio','vraggio@yahoo.com','Experto','$5$rounds=535000$84iUxdIrSBxoXCZ8$2dI5W0ub/5HDxxgG9FgPxL6v2fE7GWvmMfHFbpy5J09',3,3,400,NULL,NULL,NULL,NULL,NULL,NULL),('lspan','Lucía','Spangenberg','lucia83@gmail.com','Avanzado','$5$rounds=535000$hTCRIpe8HMd5r9jz$2Gk2kNmCLFaDQFNesweX7ArsNipF7zNGuv4IsJNUcp9',1,3,250,NULL,NULL,NULL,NULL,NULL,NULL),('mbrandes','Mariana','Brandes','brandesmariana@gmail.com','Competente','$5$rounds=535000$vpUV2i5zHBDuFbAv$O2/4blr1M8hTlkq2ITBxpFzvKNwib0RdZNjPTNw2O26',1,2,150,NULL,NULL,NULL,NULL,NULL,NULL),('hnaya','Hugo','Naya','naya@pasteur.edu.uy','Experto','$5$rounds=535000$Nx2PiG7SXGZ2meWG$8Ir.je0jJMsquWwRGExRF6oFDnBj99a2dEWsxelRsr9',1,3,250,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

