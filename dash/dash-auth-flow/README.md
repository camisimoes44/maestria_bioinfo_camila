# Project structure

This project contains the implementation of actG-Learn platform, used in the context of human genome variant interpretation, for variant labeling and user learning.

The project is organised in two parts:
- *app/*: web application (Dash: dash-auth-flow)
- *app_server/*: web application server (Flask/MySQL)

For installation details and requirements of each part, please refer to the README files on each folder. 