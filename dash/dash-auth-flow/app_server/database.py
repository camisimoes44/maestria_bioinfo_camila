#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script is intended to contain all the functions to communicate with the MySQL database
"""

import MySQLdb
import config
import simplejson


def mysql_execute_query(query):
    """
    Execute a MySQL query with the configured connection
    :param query: MySQL query to execute
    :return: query execution status, query result (JSON)/ row count (int)
    """
    try:
        db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                                  port=config.db_port)
    except MySQLdb.OperationalError as e:
        print(e)
        return 'error', 'OperationalError'

    try:
        cursor = db_conn.cursor()
        cursor.execute(query)

        if query.upper().startswith('SELECT'):
            # SELECT queries (must return result as JSON)
            db_conn.close()
            data_json = mysql_result_to_json(cursor)
            data = data_json
        else:
            # other queries (must return affected row count)
            db_conn.commit()
            db_conn.close()
            data = cursor.rowcount
        return 'ok', data

    except MySQLdb.DataError as e:
        print(e)
        return 'error', "DataError"

    except MySQLdb.InternalError as e:
        print(e)
        return 'error', "InternalError"

    except MySQLdb.IntegrityError as e:
        print(e)
        return 'error', "IntegrityError"

    except MySQLdb.OperationalError as e:
        print(e)
        return 'error', "OperationalError"

    except MySQLdb.NotSupportedError as e:
        print(e)
        return 'error', "NotSupportedError"

    except MySQLdb.ProgrammingError as e:
        print(e)
        return 'error', "ProgrammingError"

    except:
        print("UnknownError")
        return 'error', "UnknownError"


def mysql_result_to_json(cursor):
    """
    Convert a MySQL result into JSON.
    Use SimpleJSON to handle Decimal values.
    :param cursor: MySQL cursor
    :return: query result (JSON)
    """
    data = cursor.fetchall()
    json_data = []
    row_headers = [x[0] for x in cursor.description]  # this will extract row headers
    for result in data:
        json_data.append(dict(zip(row_headers, result)))
    #print(json_data)
    return simplejson.dumps(json_data)  # use simplejson to handle Decimal values


def list_non_conflictive_variants(user):
    """
    List some attributes of all the non-conflictive variants in the database
    :return: query result (JSON)
    """

    dificulty = variants_given_level(user)
    dificulty = str(dificulty).replace('[', '(')
    dificulty = dificulty.replace(']', ')')


    query = "SELECT ID, Chr, Start, End, Ref, Alt, `Gene.refGene`,`AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `avsnp150`, `freq.max`" \
            "FROM variants " \
            "WHERE CLNSIG IS NOT NULL AND is_conflict=0 AND dificulty="+str(dificulty)
    return mysql_execute_query(query)
    
    
def list_conflictive_variants():
    """
    List some attributes of all the conflictive variants in the database
    :return: query result (JSON)
    """
    query = "SELECT ID, Start, End, Ref, Alt, `AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `Gene.refGene` " \
            "FROM variants " \
            "WHERE CLNSIG IS NOT NULL AND is_conflict=1"
    return mysql_execute_query(query)


def list_labels(is_classif):
    """
    List all labels in the database
    :return: query result (JSON)
    """
    if str(is_classif) == '1':
        query = "SELECT * FROM labels WHERE is_classif=1"
    else:
        query = "SELECT * FROM labels WHERE is_training=1"
    return mysql_execute_query(query)

def list_comments():
    """
    List all labels in the database
    :return: query result (JSON)
    """
    query = "SELECT * FROM comments"
    return mysql_execute_query(query)


def list_rules():
    """
    List all labels in the database
    :return: query result (JSON)
    """
    query = "SELECT * FROM rules"
    return mysql_execute_query(query)


def list_levels():
    """
    List all expertise levels in the database
    :return: query result (JSON)
    """
    query = "SELECT * FROM expertise_levels"
    return mysql_execute_query(query)


def get_variant(id):
    """
    Get the data of a variant form the database
    :param id: ID of variant (integer)
    :return: query result (JSON)
    """
    query = "SELECT * FROM variants WHERE ID=" + str(id)
    return mysql_execute_query(query)

def get_sample(id):
    """
    Get the data of a sample form the database
    :param id: ID of variant (integer)
    :return: query result (JSON)
    """
    query = "SELECT * FROM sample_information WHERE sample='" + str(id)+"'"
    return mysql_execute_query(query)



def classification_times(id):
    """
    Get the times a variant was classificated
    :param id: ID of variant (integer)
    :return: query result (JSON)
    """
    query = "SELECT count(*) FROM user_classification WHERE variant_ID=" + str(id)
    return mysql_execute_query(query)

def list_classification_times(id):
    """
    Get the data of each classificated variant
    :param id: ID of variant (integer)
    :return: query result (JSON)
    """
    #query = "SELECT first, last, label_ID, timestamp FROM (user_classification JOIN users ON user_classification.user_ID=users.user) WHERE variant_ID=" + str(id)
    query = "SELECT user_ID, level, DATE_FORMAT(timestamp, '%d/%m/%Y %H:%i:%S'),label_ID, COMMENT FROM (user_classification JOIN users ON user_ID = user JOIN comments ON comment_ID = ID) WHERE variant_ID="+str(id)

    return mysql_execute_query(query)

   #print('interseccion', tipo)

def list_training_times(id):
    """
        Get the data of all the trainings made by a user
        :param id: ID of user (varchar)
        :return: query result (JSON)
        """
    query = "SELECT level, XP FROM users WHERE user='"+str(id)+"'"
    print(query)
    return mysql_execute_query(query)

def list_classif_times(id):
    """
        Get the data of all the trainings made by a user
        :param id: ID of user (varchar)
        :return: query result (JSON)
        """
    query = "SELECT level, XP, number_of_classifications FROM users WHERE user='"+str(id)+"'"
    print(query)
    return mysql_execute_query(query)


def get_random_variant(conflict, user, asked_type):
    """
    Get the data of a random variant form the database
    :return: query result (JSON)
    """
    allowed_variants = variants_given_access(user)
    #print('asked', asked_type, type(asked_type))
    #print('allowed', allowed_variants, type(allowed_variants))
    tipo = [value for value in asked_type if value in allowed_variants]
    tipo = str(tipo).replace('[', '(')
    tipo = tipo.replace(']', ')')
    dificulty = variants_given_level(user)
    dificulty = str(dificulty).replace('[', '(')
    dificulty = dificulty.replace(']', ')')


    if conflict == 0:
        #training
        query = "SELECT * FROM variants JOIN (SELECT ID FROM variants WHERE is_conflict="+str(conflict)+" AND tipo=1 AND dificulty IN"+str(dificulty)+"ORDER BY RAND() LIMIT 1) as rand ON variants.ID=rand.ID"
    elif conflict == 1:
        #labeling
        query = "SELECT * FROM variants JOIN (SELECT ID FROM variants WHERE is_conflict="+str(conflict)+" AND tipo IN"+str(tipo)+" ORDER BY RAND() LIMIT 1) as rand ON variants.ID=rand.ID"
    return mysql_execute_query(query)


def get_submission_info(id):
    """
    Get the information of variant submission in ClinVar
    :return: query result (JSON)
    """
    query = "SELECT * FROM submissions WHERE ID=" + str(id)
    return mysql_execute_query(query)

def get_conflict_info(id):
    """
    Get the information of the conflict generated by the variant
    :return: query result (JSON)
    """
    query = "SELECT * FROM conflict_info WHERE ID=" + str(id)
    return mysql_execute_query(query)



def set_user_classification(user_id, variant_id, label_id, comment_id, timestamp):
    """
    Save to database a classification of a variant, performed by a user
    :param user_id: author of classification
    :param variant_id: variant classified
    :param label_id: label assigned to variant
    :param is_correct: result of classification
    :return:
    """
    query = "INSERT INTO user_classification (user_ID, variant_ID, label_ID, comment_ID, timestamp) VALUES ('" + str(user_id) \
            + "', " + str(variant_id) + ", '" + str(label_id) + "', " + str(comment_id) + ", '" + str(timestamp) +"') "
    # print(query)
    return mysql_execute_query(query)

def set_consensus(consensus_id, variant_id, consensus, confidence_level, agreement_level, timestamp):
    query = "INSERT INTO consensus (consensus_ID, variant_ID, consensus, confidence_level, agreement_level, timestamp) VALUES (" + str(consensus_id) \
            + ", " + str(variant_id) + ", '" + str(consensus) + "', " + str(confidence_level) + ", '" + str(agreement_level) + "', '" + str(timestamp)+ "') "
    print(query)
    return mysql_execute_query(query)

def update_xp_level(user, level, XP):
    query = "UPDATE users SET level='" + str(level)+"', XP=" + str(XP) + " WHERE user='" + str(user) + "'"
    print(query)
    return mysql_execute_query(query)



def set_last_classification(organization_id, timestamp):
    query = "UPDATE users_organization SET date_last_submitted ='" + str(timestamp) + "' WHERE organization = " +str(organization_id)
    #print("Hola query1", query)
    return mysql_execute_query(query)

def set_classification_count(organization_id):
    query_inst = "SELECT count(*) FROM user_classification,users WHERE user_ID = user AND institution = " + str(organization_id)
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_inst)
    db_conn.close()
    classification_count = cursor.fetchone()[0]
    query = "UPDATE users_organization SET number_of_classifications =" + str(classification_count) + " WHERE organization = " +str(organization_id)
    #print("Hola query3", query)
    return mysql_execute_query(query)

def set_classification_count_user(user_id):
    query_count = "SELECT count(*) FROM user_classification WHERE user_ID ='" + str(user_id)+"'"
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_count)
    db_conn.close()
    classification_count = cursor.fetchone()[0]
    query = "UPDATE users SET number_of_classifications =" + str(classification_count) + " WHERE user = '" +str(user_id)+"'"
    return mysql_execute_query(query)

def set_training_count_user(user_id):
    query_count = "SELECT count(*) FROM user_training WHERE user_ID ='" + str(user_id)+"'"
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_count)
    db_conn.close()
    classification_count = cursor.fetchone()[0]
    query = "UPDATE users SET number_of_trainings =" + str(classification_count) + " WHERE user = '" +str(user_id)+"'"
    return mysql_execute_query(query)



def variants_given_access(user):
    query = "SELECT access_level FROM users WHERE user ='"+str(user)+"'"
    access_level = mysql_execute_query(query)
    access_level = access_level[1].replace('[', '')
    access_level = access_level.replace(']', '')
    access_level = simplejson.loads(access_level)['access_level']

    if access_level == 3:
        variant_levels = [1,2,3]
    elif access_level == 2:
        variant_levels = [1, 2]
    else:
        variant_levels = [1]

    return variant_levels

def variants_given_level(user):
    query = "SELECT level FROM users WHERE user ='"+str(user)+"'"
    level = mysql_execute_query(query)
    level = level[1].replace('[', '')
    level = level.replace(']', '')
    level = simplejson.loads(level)['level']

    if level == 'Novato':
        variant_levels = [1]
    elif level == 'Principiante':
        variant_levels = [2]
    elif level == 'Competente':
        variant_levels = [3]
    elif level == 'Avanzado':
        variant_levels = [4]
    else:
        variant_levels = [5]
    return variant_levels


def list_conflictive_variants(user, asked_type, asked_patient):
   """
   List some attributes of all the conflictive variants in the database
   :return: query result (JSON)
   """
   allowed_variants = variants_given_access(user)
   #print('asked', asked_type, type(asked_type))
   #print('allowed', allowed_variants, type(allowed_variants))
   tipo = [value for value in asked_type if value in allowed_variants]
   tipo = str(tipo).replace('[','(')
   tipo = tipo.replace(']',')')
   #print('interseccion', tipo)
   patient = asked_patient.replace('[','(')
   patient = patient.replace(']',')')
   print('asked', patient)
   if patient == '()':
       query = "SELECT ID, Chr, Start, End, Ref, Alt, `Gene.refGene`,`AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `avsnp150`, `freq.max`" \
               "FROM variants " \
               "WHERE tipo IN " + str(tipo)
   else:
       query = "SELECT ID, Chr, Start, End, Ref, Alt, `AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `Gene.refGene` " \
               "FROM variants " \
               "WHERE tipo IN " + str(tipo) + " AND patient IN " + str(patient)

   return mysql_execute_query(query)


def list_conflictive_variants_part(user, asked_type, asked_patient):
   """
   List some attributes of all the conflictive variants in the database
   :return: query result (JSON)
   """
   allowed_variants = variants_given_access(user)
   #print('asked', asked_type, type(asked_type))
   #print('allowed', allowed_variants, type(allowed_variants))
   tipo = [value for value in asked_type if value in allowed_variants]
   tipo = str(tipo).replace('[','(')
   tipo = tipo.replace(']',')')
   #print('interseccion', tipo)
   patient = asked_patient.replace('[','(')
   patient = patient.replace(']',')')
   print('asked', patient)
   if patient == '()':
       query = "SELECT ID, Chr, Start, End, Ref, Alt, `Gene.refGene`,`AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `avsnp150`, `freq.max`" \
               "FROM variants " \
               "WHERE tipo IN " + str(tipo) + "ORDER BY RAND() LIMIT 10"
   else:
       query = "SELECT ID, Chr, Start, End, Ref, Alt, `AAChange.refGene`, `Func.refGene`, `ExonicFunc.refGene`, `Gene.refGene` " \
               "FROM variants " \
               "WHERE tipo IN " + str(tipo) + " AND patient IN " + str(patient) + "ORDER BY RAND() LIMIT 10"

   return mysql_execute_query(query)

def list_conflictive_patients(user, asked_type):
   """
   List patients of all the conflictive variants type 3 in the database
   :return: query result (JSON)
   """
   allowed_variants = variants_given_access(user)
   #print('asked', asked_type, type(asked_type))
   #print('allowed', allowed_variants, type(allowed_variants))
   tipo = [value for value in asked_type if value in allowed_variants]
   tipo = str(tipo).replace('[','(')
   tipo = tipo.replace(']',')')
   #print('interseccion', tipo)

   query = "SELECT patient " \
           "FROM variants " \
           "WHERE tipo IN "+str(tipo)
   return mysql_execute_query(query)

def update_organization_classification_stats(user_id,timestamp):
    query_inst = "SELECT institution FROM users WHERE user ='" + str(user_id) + "'"
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_inst)
    db_conn.close()
    organization_id = cursor.fetchone()[0]
    set_last_classification(organization_id, timestamp)
    set_classification_count(organization_id)



def set_user_training(user_id, variant_id, label_id, is_correct, timestamp):
    """
    Save to database a classification of a variant, performed by a user
    :param user_id: author of classification
    :param variant_id: variant classified
    :param label_id: label assigned to variant
    :param is_correct: result of classification
    :return:
    """
    query = "INSERT INTO user_training (user_ID, variant_ID, label_ID, is_correct, timestamp) VALUES ('" + str(user_id) \
            + "', " + str(variant_id) + ", '" + str(label_id) + "', " + str(is_correct) + ", '" +str(timestamp)+ "') "
    # print(query)
    return mysql_execute_query(query)

def set_last_training(organization_id, timestamp):
    query = "UPDATE users_organization SET date_last_trained ='" + str(timestamp) + "' WHERE organization = " +str(organization_id)
    return mysql_execute_query(query)

def set_training_count(organization_id):
    query_inst_t = "SELECT count(*) FROM user_training,users WHERE user_ID = user AND institution = " + str(organization_id)
    print("Hola query_inst train", query_inst_t)
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_inst_t)
    db_conn.close()
    classification_count = cursor.fetchone()[0]
    query = "UPDATE users_organization SET number_of_training =" + str(classification_count) + " WHERE organization = " +str(organization_id)
    return mysql_execute_query(query)

def update_organization_training_stats(user_id,timestamp):
    query_inst_t = "SELECT institution FROM users WHERE user ='" + str(user_id) + "'"
    db_conn = MySQLdb.connect(host=config.db_host, user=config.db_user, passwd=config.db_passwd, db=config.db_name,
                              port=config.db_port)
    cursor = db_conn.cursor()
    cursor.execute(query_inst_t)
    db_conn.close()
    organization_id = cursor.fetchone()[0]
    set_last_training(organization_id, timestamp)
    set_training_count(organization_id)



def get_user_data(user):
    """
    Get the data of a user from database
    :param user: user name (string)
    :return: query result (JSON)
    """
    query = "SELECT * FROM users WHERE user='" + user + "';"
    return mysql_execute_query(query)


def get_user_stats(user):
    """
    Get the statistics of a user from database
    :param user: user name (string)
    :return: query result (JSON)
    """
    # query = "SELECT * FROM user_classification WHERE user='" + user + "';"
    query = "SELECT is_correct, COUNT(*) AS count"\
        " FROM user_classification"\
        " WHERE user_ID = '" + user + "'"\
        " GROUP BY is_correct;"

    return mysql_execute_query(query)
