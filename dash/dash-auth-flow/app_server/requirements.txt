Flask==1.1.2
gunicorn==20.0.4
mysqlclient==2.0.1
simplejson==3.17.2
passlib==1.7.4
flask-swagger-ui==3.36.0
pdoc3==0.9.2
