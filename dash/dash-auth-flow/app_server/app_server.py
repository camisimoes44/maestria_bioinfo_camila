#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Main script of the server, used to publish all the routes of the API
"""

from flask import Flask, render_template, request
from passlib.hash import sha256_crypt
import json
from flask_swagger_ui import get_swaggerui_blueprint

import config
import database as db
import datetime
import subprocess
import pandas as pd
import os

server = Flask(__name__)
api_v1_base = '/api/v1'

### swagger specific ###
SWAGGER_URL = api_v1_base + '/doc'
SWAGGER_CONFIG_URL = '/static/swagger_api_v1.yaml'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    SWAGGER_CONFIG_URL,
    config={
        'app_name': "actG-Learn"
    }
)
server.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
### end swagger specific ###

def get_current_datetime():
    """
    Get the current datetime with format dd/mm/yy HH:MM:SS.ms

    :return: (string) current datetime
    """
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")

print(get_current_datetime())

def generate_json_response(status, data):
    """
    Generate a JSON response to be returned by the services
    :param status: status of the response, i.e.: ok, error
    :param data: data of the response, i.e.: JSON, error type, etc
    :return: response (JSON)
    """
    return {"status": status, "data": data}


def represents_int(value):
    """
    Check if the value represents an integer
    :param value: value to check
    :return: boolean
    """
    try:
        int(value)
        return True
    except ValueError:
        return False

def xp_to_level(xp):
    if xp in range(0,49):
        level = 'Novato'
    elif xp in range(50,149):
        level = 'Principiante'
    elif xp in range(150,249):
        level = 'Competente'
    elif xp in range(250,399):
        level = 'Avanzado'
    elif xp in range(400,550):
        level = 'Experto'
    return level

@server.route(api_v1_base + '/users/training/set', methods=['POST'])
def set_training():
    """
    Save a classification of a variant
    :return: response (JSON)
    """
    if 'user_id' in request.form and 'variant_id' in request.form and \
            'label_id' in request.form and 'is_correct' in request.form:
        variant_id = request.form['variant_id']
        user_id = request.form['user_id']
        label_id = request.form['label_id']
        is_correct = request.form['is_correct']
        current_datetime = get_current_datetime()
        status, data = db.set_user_training(user_id, variant_id, label_id, is_correct, current_datetime)
        db.update_organization_training_stats(user_id, current_datetime)
        db.set_training_count_user(user_id)
        # print(user_id, variant_id, label_id, is_correct)
        if status == 'ok':
            # classification was inserted
            data = 'The classification was saved!'
    else:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/consensus/set', methods=['POST'])
def set_consensus():
    """
        Update a consensus label
        :return: response (JSON)
    """
    if 'variant_id' in request.form:
        variant_id = request.form['variant_id']
        status, data = db.list_classification_times(variant_id)
        json_data = json.loads(data)

        df_list = list()
        for item in json_data:
            df_list.append(item)
        df = pd.DataFrame(df_list)
        labels = ['Benign', 'Conflicting', 'Conflicting', 'Likely Pathogenic', 'Pathogenic', 'Response to drugs','Risk factor', 'VUS']
        #df = pd.DataFrame.from_dict(json_data, orient="index")
        vote_count = {}
        if 'Experto' in df['level'].values.tolist():
            exp = df.loc[df['level'] == 'Experto']
            for label in exp['label_ID']:
                if label not in vote_count:
                    vote_count[label] = 0
                vote_count[label] += 1

            max_count = 0
            for label, count in vote_count.items():
                if count > max_count:
                    max_label = label
                    max_count = count
            # Return the consensus label
            if len(vote_count) > 1 and len(set(vote_count.values())) == 1:
                consensus = "Conflicting"
            else:
                consensus = max_label
            confidence_level = 1
        elif any(item in ['Competente', 'Avanzado'] for item in df['level'].values.tolist()):
            ca = df.loc[df['level'].isin(['Competente', 'Avanzado'])]
            for label in ca['label_ID']:
                if label not in vote_count:
                    vote_count[label] = 0
                vote_count[label] += 1
            max_count = 0
            for label, count in vote_count.items():
                if count > max_count:
                    max_label = label
                    max_count = count
            # Return the consensus label
            if len(vote_count) > 1 and len(set(vote_count.values())) == 1:
                consensus = "Conflicting"
            else:
                consensus = max_label
            confidence_level = 0.7
        else:
            for label in df['label_ID']:
                if label not in vote_count:
                    vote_count[label] = 0
                vote_count[label] += 1
            max_count = 0
            for label, count in vote_count.items():
                if count > max_count:
                    max_label = label
                    max_count = count
            # Return the consensus label
            if len(vote_count) > 1 and len(set(vote_count.values())) == 1:
                consensus = "Conflicting"
            else:
                consensus = max_label
            confidence_level = 0.3

        agreement_level =0
        current_datetime = get_current_datetime()
        consensus_id =  variant_id
        status, data = db.set_consensus(consensus_id, variant_id, consensus, confidence_level, agreement_level, current_datetime)

    else:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/users/points/training', methods=['POST'])
def update_user_points():
    if 'user_id' in request.form and 'is_correct' in request.form:
        user_id = request.form['user_id']
        is_correct = request.form['is_correct']
        print(user_id, is_correct)
        status, data = db.list_training_times(user_id)
        json_data = json.loads(data)
        df_list = list()
        for item in json_data:
            df_list.append(item)
        df = pd.DataFrame(df_list)

        if is_correct == '1':
            print('holi')
            XP_nuevo = df.iloc[0]['XP']+5
        else:
            XP_nuevo = df.iloc[0]['XP']
        level_nuevo = xp_to_level(XP_nuevo)
        print(XP_nuevo, level_nuevo)
        #current_datetime = get_current_datetime()
        status, data = db.update_xp_level(user_id, level_nuevo, XP_nuevo)

    else:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)

@server.route(api_v1_base + '/users/points/classification', methods=['POST'])
def update_user_points_classification():
    if 'user_id' in request.form:
        user_id = request.form['user_id']
        status, data = db.list_classif_times(user_id)
        json_data = json.loads(data)
        df_list = list()
        for item in json_data:
            df_list.append(item)
        df = pd.DataFrame(df_list)

        if df.iloc[0]['number_of_classifications'] % 5 == 0:
            XP_nuevo = df.iloc[0]['XP'] + 5
        else:
            XP_nuevo = df.iloc[0]['XP']
        level_nuevo = xp_to_level(XP_nuevo)
        #current_datetime = get_current_datetime()
        status, data = db.update_xp_level(user_id, level_nuevo, XP_nuevo)
    else:
        status = 'error'
        data = 'RequestError'
    return generate_json_response(status, data)



@server.route(api_v1_base + '/users/classifications/set', methods=['POST'])
def set_classification():
    """
    Save a classification of a variant
    :return: response (JSON)
    """
    if 'user_id' in request.form and 'variant_id' in request.form and \
            'label_id' in request.form and 'comment_id' in request.form:
        variant_id = request.form['variant_id']
        user_id = request.form['user_id']
        label_id = request.form['label_id']
        comment = request.form['comment_id']
        current_datetime = get_current_datetime()
        status, data = db.set_user_classification(user_id, variant_id, label_id, comment, current_datetime)
        db.update_organization_classification_stats(user_id, current_datetime)
        db.set_classification_count_user(user_id)


        if status == 'ok':
            # classification was inserted
            data = 'The classification was saved!'
    else:
        status = 'error'
        data = 'RequestError'
    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/nonconf', methods=['POST'])
def list_non_conflictive_variants():
    """
    List all the non-conflictive variants in the database
    :return: response (JSON)
    """

    if 'user' in request.form:
        user = request.form['user']

    status, data = db.list_non_conflictive_variants(user)
    return generate_json_response(status, data)


#@server.route(api_v1_base + '/variants/conf', methods=['GET'])
#def list_conflictive_variants():
#    """
#    List all the conflictive variants in the database
#    :return: response (JSON)
#    """
#    status, data = db.list_conflictive_variants()
#    return generate_json_response(status, data)


@server.route(api_v1_base + '/labels', methods=['POST'])
def list_labels():
    """
    List all the labels in the database
    :return: response (JSON)
    """
    if 'is_classif' in request.form:
        is_classif = request.form['is_classif']
        status, data = db.list_labels(is_classif)
        # print(user_id, variant_id, label_id, is_correct)
    else:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)

@server.route(api_v1_base + '/variants/conf', methods=['POST'])
def list_conflictive_variants():
    """
    List all the conflictive variants in the database
    :return: response (JSON)
    """
    if 'user' in request.form:
        user = request.form['user']
    if 'selection' in request.form:
        selection = request.form['selection'] #TODO: probar método request.form.getlist('selection')
        selection = selection.replace('[', '')
        selection = selection.replace(']', '')
        selection = selection.replace(' ', '')
        selection = list(selection.split(','))
        selection = [int(i) for i in selection]
    if 'selection_2' in request.form:
        selection_2 = request.form['selection_2']

    status, data = db.list_conflictive_variants(user, selection, selection_2)
    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/conf/part', methods=['POST'])
def list_conflictive_variants_part():
    """
    List all the conflictive variants in the database
    :return: response (JSON)
    """
    if 'user' in request.form:
        user = request.form['user']
    if 'selection' in request.form:
        selection = request.form['selection'] #TODO: probar método request.form.getlist('selection')
        selection = selection.replace('[', '')
        selection = selection.replace(']', '')
        selection = selection.replace(' ', '')
        selection = list(selection.split(','))
        selection = [int(i) for i in selection]
    if 'selection_2' in request.form:
        selection_2 = request.form['selection_2']

    status, data = db.list_conflictive_variants_part(user, selection, selection_2)
    return generate_json_response(status, data)

@server.route(api_v1_base + '/variants/conf/patient', methods=['POST'])
def list_conflictive_patient():
    """
    List all the patients in conflictive variants in the database
    :return: response (JSON)
    """
    if 'user' in request.form:
        user = request.form['user']
    if 'selection' in request.form:
        selection = request.form['selection'] #TODO: probar método request.form.getlist('selection')
        selection = selection.replace('[', '')
        selection = selection.replace(']', '')
        selection = selection.replace(' ', '')
        selection = list(selection.split(','))
        selection = [int(i) for i in selection]

    status, data = db.list_conflictive_patients(user, selection)
    return generate_json_response(status, data)

@server.route(api_v1_base + '/comments', methods=['GET'])
def list_comments():
    """
    List all the labels in the database
    :return: response (JSON)
    """
    status, data = db.list_comments()
    return generate_json_response(status, data)


@server.route(api_v1_base + '/rules', methods=['GET'])
def list_rules():
    """
    List all the rules in the database
    :return: response (JSON)
    """
    status, data = db.list_rules()
    return generate_json_response(status, data)


@server.route(api_v1_base + '/levels', methods=['GET'])
def list_levels():
    """
    List all the expertise levels in the database
    :return: response (JSON)
    """
    status, data = db.list_levels()
    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/<int:variant_id>', methods=['GET'])
def get_variant(variant_id):
    """
    Get all the attributes of a specific or a random variant
    :param: variant_id
    :return: response (JSON)
    """
    error = False
    # check if the received ID is an integer, otherwise return an error
    if represents_int(variant_id):
        variant_id = int(variant_id)
        if variant_id > 0:
            # specific variant
            print('> Requested variant:', variant_id)
            status, data = db.get_variant(variant_id)
            status2, data2 = db.classification_times(variant_id)
            data = data[:-2] + ', ' + data2[2:-2] + data[-2:]
        else:
            error = True
    else:
        error = True

    if error:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)

@server.route(api_v1_base + '/sample/<sample_id>', methods=['GET'])
def get_sample(sample_id):
    """
    Get all the attributes of samplee
    :param: sample_id
    :return: response (JSON)
    """
    status, data = db.get_sample(str(sample_id))
    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/times/<int:variant_id>', methods=['GET'])
def get_clasif_times(variant_id):
    """
    Get all the attributes of a specific or a random variant
    :param: variant_id
    :return: response (JSON)
    """
    error = False
    # check if the received ID is an integer, otherwise return an error
    if represents_int(variant_id):
        variant_id = int(variant_id)
        if variant_id > 0:
            # specific variant
            print('> Requested variant:', variant_id)
            status, data = db.list_classification_times(variant_id)

        else:
            error = True
    else:
        error = True

    if error:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/nonconf/random', methods=['POST'])
def get_nonconf_random_variant():
    """
    Get all the attributes of a non conflicting random variant
    :return: response (JSON)
    """
    print('> Requested a random variant')

    if 'user' in request.form:
        user = request.form['user']

    if 'selection' in request.form:
        selection = request.form['selection'] #TODO: probar método request.form.getlist('selection')
        selection = selection.replace('[', '')
        selection = selection.replace(']', '')
        selection = selection.replace(' ', '')
        selection = list(selection.split(','))
        selection = [int(i) for i in selection]

    status, data = db.get_random_variant(0, user, selection)

    start = data.find('ID": ') + len('ID": ')
    end = data.find(', "ID_int"')
    substring = data[start:end]
    status2, data2 = db.classification_times(substring)
    data = data[:-2] + ', ' + data2[2:-2] + data[-2:]

    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/conf/random', methods=['POST'])
def get_conf_random_variant():
    """
    Get all the attributes of a conflicting random variant
    :return: response (JSON)
    """

    if 'user' in request.form:
        user = request.form['user']
    if 'selection' in request.form:
        selection = request.form['selection'] #TODO: probar método request.form.getlist('selection')
        selection = selection.replace('[', '')
        selection = selection.replace(']', '')
        selection = selection.replace(' ', '')
        selection = list(selection.split(','))
        selection = [int(i) for i in selection]

    status, data = db.get_random_variant(1,user, selection)

    start = data.find('ID": ') + len('ID": ')
    end = data.find(', "ID_int"')
    substring = data[start:end]
    status2, data2 = db.classification_times(substring)
    data = data[:-2] + ', ' + data2[2:-2] + data[-2:]

    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/conflict/<int:variant_id>', methods=['GET'])
def get_conflict(variant_id):
    """
        Get all the attributes of the conflict generated in a variant
        :return: response (JSON)
    """
    error = False
    # check if the received ID is an integer, otherwise return an error
    if represents_int(variant_id):
        variant_id = int(variant_id)
        if variant_id > 0:
            status, data = db.get_conflict_info(variant_id)
        else:
            error = True
    else:
        error = True

    if error:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/variants/clin_submission/<int:variant_id>', methods=['GET'])
def get_submision(variant_id):
    """
        Get all the attributes of variant submission in ClinVar
        :return: response (JSON)
    """
    error = False
    # check if the received ID is an integer, otherwise return an error
    if represents_int(variant_id):
        variant_id = int(variant_id)
        if variant_id > 0:
            status, data = db.get_submission_info(variant_id)
        else:
            error = True
    else:
        error = True

    if error:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/users/login', methods=['POST'])
def login():
    """
    Validate user and password to login
    :return: response (JSON)
    """
    if 'user' in request.form and 'password' in request.form:
        user = request.form['user']
        user_password = request.form['password']

        # print(sha256_crypt.hash(user_password))
        # password2 = sha256_crypt.hash("password")

        # print(password)
        # print(password2)
        # print(sha256_crypt.verify(password, password2))

        status, data = db.get_user_data(user)
        if data is None or data == '[]':
            # user does not exist
            print('>>> LoginError:', user, '. Wrong user.')
            status = 'error'
            data = 'LoginError'
        else:
            json_user_data = json.loads(data)[0]
            hashed_password_db = json_user_data['password']
            if sha256_crypt.verify(user_password, hashed_password_db):
                # password OK
                print('>>> LoginOK:', user)
                del json_user_data['password']  # delete the hashed password from the response
                status = 'ok'
                data = json_user_data
            else:
                # wrong password
                print('>>> LoginError:', user, '. Wrong password.')
                status = 'error'
                data = 'LoginError'
    else:
        status = 'error'
        data = 'RequestError'

    return generate_json_response(status, data)


@server.route(api_v1_base + '/users/stats/<username>', methods=['GET'])
def get_user_stats(username):
    status, data = db.get_user_stats(str(username))
    return generate_json_response(status, data)

@server.route(api_v1_base + '/training/start', methods=['POST'])
def start_training():
    """

    """
    print('antes del coso')
    proc = subprocess.Popen(['python', 'prueba.py'], shell=False, stdin=None, stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE)
    #os.system("python3 prueba.py &")
    print('despues del coso')
    print(proc.pid)

    status = 'Training'
    data = 'Training'

    return generate_json_response(status, data)

if __name__ == "__main__":
    server.run(debug=True, host=config.server_ip, port=config.server_port)
