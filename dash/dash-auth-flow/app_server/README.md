# Server
## Information
Flask API server with web services and persistence in MySQL.

## Requirements
Developed with:
- Python 3
- MySQL 8

## Deploy:
1. Create and activate a virtual environment 'venv' with Python 3
	1. `python3 -m venv venv`
	2. `source venv/bin/activate`
2. Install requirements:
	- `pip install -r requirements.txt`
3. Setup database:
    1. Log into MySQL with a privileged user: `mysql -u root -p`
    2. Recover structure and data:
        1. `source bioinfo_structure.sql`
        2. `source variants.sql`
        3. `source conflict_info.sql`
        4. `source organizations.sql`
        5. `source submissions.sql`
        6. `source other_tables.sql`
4. Create config from example:
    - `cp config.example.py config.py`
5. Configure variables in config.py.
6. Run server:
	- Development:
		- `python app_server.py`
	- Production:
		- `gunicorn --bind 0.0.0.0:5000 app_server:server`
		
## Documentation
### API
- Generated with [Swagger](https://swagger.io)
- Please go to `<IP address>:<port>/api/<api version>/doc`
    - Example: http://127.0.0.1:5000/api/v1/doc/
### Code
- Generated with [pdoc](https://pdoc3.github.io/pdoc)
1. Run: `pdoc3 --html <script>`
    - Example: `pdoc3 --html app_server.py`
2. Open output file.
    - Example: `app_server.html`