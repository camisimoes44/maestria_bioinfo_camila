#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script to generate hashes of user passwords

How to use it:
    1. Add passwords to the 'passwords' list.
    2. Run the script.
    3. Use the generated hashes to set the user's hash on the database.
"""

from passlib.hash import sha256_crypt

print('password\thash')
passwords = ['peDzHw9kxHjAjn', 'JFkdBB2MScwF', 'Jv6NatfWAfdr', 'XyKLBaEdu6gy', 'hax8amqKgxHg', 'c56ndUYaWPtE', 'xWGw55UEmTQT', 'mqHnjG56KVMy', 's6pT6jZ9CRHe', 'm7nNUPD2Nv6J', '', '9fx9eHkaBGsa', 'u85R5eM2dYh4', 'p43ce9Ak2us4', 'Eu68GR6Wgj6b', 'C8PKYLdMgAs4', '8QfsPp4WV8Kn', 'F3gPkNfX3d5f', 'aJpwP8UPnNPr']
for p in passwords:
    print(str(p) + '\t' + sha256_crypt.hash(p))