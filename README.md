This repository contains projects developed in the context of a master's thesis. 
It contains 3 projects, with different levels of development:

1. dash: contains the implementation of the actG-Learn platform (in dash_auth_flow).
2. shiny: contains initial tests of a dashboard performed in Shiny. 
3. streamlit: contains initial tests of a dashboard performed in Streamlit. 